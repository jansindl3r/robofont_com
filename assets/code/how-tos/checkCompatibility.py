from mojo.UI import SelectFont

neutral = SelectFont()

for f in AllFonts():

    if f == neutral:
        continue

    for g in f:
        compatible, report = g.isCompatible(neutral[g.name])

        if compatible:
            f[g.name].markColor = 0, 1, 0, 0.5
        else:
            f[g.name].markColor = 1, 0, 0, 0.5

    f.changed()