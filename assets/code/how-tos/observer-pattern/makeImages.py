# from importlib import reload
# import arrow
# reload(arrow)

import random
import colorsys
from arrow import Arrow

random.seed(8)

t = 36
w, h = 250, t * 6

c1 = 1, 0, 0.5
c2 = 0, 0.5, 1
c3 = 0, 1, 0
sw2 = 6
sw3 = 4

def drawObject(objectName, posSize):
    save()
    font('RoboType-Bold')
    fontSize(t)
    lineHeight(h)
    fill(1)
    stroke(*c2)
    strokeWidth(sw2)
    oval(*posSize)
    stroke(None)
    fill(*c1)
    textBox(objectName, posSize, align='center')
    restore()

def drawArrow(p1, p2, dx=40, start=False, alpha=1.0):
    x1, y1 = p1
    x2, y2 = p2
    x1 += dx
    x2 -= dx
    c = makeLighterColor(c3, alpha) if alpha < 1 else c3
    A = Arrow()
    A.tipLength = 10
    A.tipAngle = 30
    A.strokeWidth = sw3
    A.color = c
    A.tipStart = True if start else False
    A.tipEnd = not A.tipStart
    A.draw((x1, y1), (x2, y2))

def makeLighterColor(color, lightness):
    H, L, S = colorsys.rgb_to_hls(*color)
    return colorsys.hls_to_rgb(H, lightness, S)

#----------
# settings
#----------

y = 70
x = 100
d = 530
x2 = x + d
x3 = x + d * 0.5
y2 = y + 200

#---------
# image 1
#---------

def image1():
    steps = 7
    py = 25
    dy = (h - (py*2)) / (steps-1)
    yPos1 = [py + y + i * dy for i in range(steps)]
    yPos2 = yPos1.copy()
    random.shuffle(yPos1)
    random.shuffle(yPos2)
    yPos = list(zip(yPos1, yPos2))

    for i in range(3):
        newPage(1000, 350)
        drawObject('interface', (x, y, w, h))
        drawObject('object', (x2, y, w, h))

        if i == 1:
            d = 30
            y1_ = y + h * 0.5 - d
            y2_ = y + h * 0.5 + d
            drawArrow((x + w, y1_), (x2, y1_), start=True)
            drawArrow((x + w, y2_), (x2, y2_), start=False)
            
        if i == 2:
            for i, (y1_, y2_) in enumerate(yPos):
                drawArrow((x + w, y1_), (x2, y2_), start=bool(i%2))

#---------
# image 2
#---------

def image2():
    for i in range(2):
        newPage(1000, 560)
        drawObject('interface', (x, y, w, h))
        drawObject('object', (x2, y, w, h))
        drawObject('dispatcher', (x3, y2, w, h))
        drawArrow((616, 266), (650, 302))
        if i:
            drawArrow((334, 302), (374, 266))

#---------
# image 3
#---------

def image3(alpha=1.0):
    removeItem = False
    newPage(1000, 560)
    drawObject('object', (x + d, y, w, h))
    drawObject('dispatcher', (x + d * 0.5, y + 200, w, h))
    h2 = h * 0.5
    fill(None)
    stroke(*c2)
    strokeWidth(sw2)
    drawArrow((616, 266), (650, 302))
    fill(1)
    oval(110, 270, h2, h2)
    oval(296, 70, h2, h2)
    c = makeLighterColor(c2, alpha) if alpha < 1 else c2
    stroke(*c)
    fill(1, 0.5)
    oval(144, 122, h2, h2)
    drawArrow((312, 348), (278, 334), start=False)
    drawArrow((378, 264), (425, 193), start=False)
    drawArrow((334, 306), (296, 224), alpha=alpha, start=False)

#---------
# image 4
#---------

def image4():

    steps = 7
    py = 25
    dy = (h - (py*2)) / (steps-1)
    yPos1 = [py + y + i * dy for i in range(steps)]
    yPos2 = yPos1.copy()
    random.shuffle(yPos1)
    random.shuffle(yPos2)
    yPos = list(zip(yPos1, yPos2))

    def drawArrows(yPos, D, w):
        for i, (Y1, Y2) in enumerate(yPos):
            drawArrow((x + w, Y1), (x2, Y2), start=bool(i%2))

    for i in range(2):
        h2 = h * 0.3
        X = x + w - h2
        Y = 60
        D = 20
        alpha = 0.9 if i else 1
        newPage(1000, 360)
        drawObject('object', (x2, y, w, h))
        drawArrows(yPos, D, w)
        fill(None)
        stroke(*c2)
        strokeWidth(sw2)
        oval(X, Y, h2, h2)
        translate(0, h2 + D)
        with savedState():
            c = makeLighterColor(c2, alpha) if alpha < 1 else c2
            stroke(*c)
            fill(1, 0.5)
            oval(X, Y, h2, h2)
        translate(0, h2 + D)
        oval(X, Y, h2, h2)

#------------
# make pages
#------------

image1()
image2()
image3()
image3(alpha=0.9)
image4()

# saveImage('observerPattern.png', multipage=True)
