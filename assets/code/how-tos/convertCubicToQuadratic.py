from lib.tools.bezierTools import curveConverter

f = CurrentFont().naked()

for g in f:
    curveConverter.bezier2quadratic(g)

f.segmentType = g.segmentType