from compositor import Font

# setup the layout engine
f = Font('/Users/gferreira/Desktop/plex-master/IBM-Plex-Serif/fonts/complete/otf/IBMPlexSerif-BoldItalic.otf')

# turn some features on
f.setFeatureState("liga", True)
f.setFeatureState("ss02", True)

# process some text
glyphRecords = f.process("koffie genieten")

# calculate the image size
pointSize = 50.0
offset = 20
s = pointSize / f.info.unitsPerEm # scale
imageWidth = sum([f[record.glyphName].width + record.xAdvance for record in glyphRecords]) * s
imageWidth = int(round(imageWidth))
imageWidth += offset * 2
imageHeight = pointSize + (offset * 2)

# setup the image
size(imageWidth, imageHeight)
translate(offset, offset)
scale(s)
translate(0, abs(f.info.descender))

# draw the glyphs
for record in glyphRecords:
    glyph = f[record.glyphName]
    pen = BezierPath()
    glyph.draw(pen)
    drawPath(pen)
    translate(record.xAdvance + glyph.width, -record.yPlacement)