from dialogKit import *

class DialogExample(object):
    
    def __init__(self):
        self.w = ModalDialog((200, 300), 'dialogKit', okCallback=self.okCallback, cancelCallback=self.cancelCallback)
        self.w.button = Button((10, 10, -10, 20), 'button', callback=self.buttonCallback)
        self.w.checkbox = CheckBox((10, 40, -10, 20), 'checkbox', callback=self.checkBoxCallback, value=True)
        self.w.list = List((10, 70, -10, 60), ['first item', 'second item'], callback=self.listSelectionCallback)
        self.w.open()
    
    def okCallback(self, sender):
        print('OK button pressed')

    def cancelCallback(self, sender):
        print('Cancel button pressed')
    
    def listSelectionCallback(self, sender):
        selection = sender.getSelection()
        if not selection:
            selectedItem = None
        else:
            selectionIndex = selection[0]
            selectedItem = sender[selectionIndex]
        print('list selection:', selectedItem)
    
    def buttonCallback(self, sender):
        print('button pressed')

    def checkBoxCallback(self, sender):
        if sender.get():        
            print('checkbox selected')
        else:
            print('checkbox deselected')

DialogExample()