from vanilla import *

class SheetDemo(object):

    def __init__(self, parentWindow):
        self.w = Sheet((200, 100), parentWindow)
        self.w.myButton = Button((10, 10, -10, 20), "a button")
        self.w.myTextBox = TextBox((10, 40, -10, 17), "a text box")
        self.w.closeButton = Button((-90, -30, 80, 22), "close", self.closeCallback)
        self.w.open()

    def closeCallback(self, sender):
        self.w.close()

class TestWindow(object):

    def __init__(self):

        self.w = Window((300, 200), title='Sheet Test')
        self.w.openSheet = Button((10, -32, -10, 22), "open sheet", callback=self.openSheetCallback)
        self.w.open()

    def openSheetCallback(self, sender):
        SheetDemo(self.w)

TestWindow()