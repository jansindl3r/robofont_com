from vanilla import Window, CheckBox, RadioGroup

class RadioGroupItemDisableExample:

    def __init__(self):

        self.w = Window((123, 140))

        self.w.allOptions = CheckBox((10, 10, -10, 22), "All Options", callback=self.allOptionsCallback)
        self.w.radioButtons = RadioGroup((10, 40, -10, 80), ["option 1", "option 2", "option 3"])

        self.w.radioButtons.getNSMatrix().cellAtRow_column_(2, 0).setEnabled_(False)

        self.w.open()

    def allOptionsCallback(self, sender):
        self.w.radioButtons.getNSMatrix().cellAtRow_column_(2, 0).setEnabled_(sender.get())

RadioGroupItemDisableExample()