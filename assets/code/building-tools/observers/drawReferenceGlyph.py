from mojo.events import addObserver
from mojo.drawingTools import *

class DrawReferenceGlyph(object):

    def __init__(self):
        addObserver(self, "drawReferenceGlyph", "draw")

    def drawReferenceGlyph(self, info):
        # get the current glyph
        glyph = info["glyph"]

        # define a fill color
        color = 0, 0, 0, 0.5

        # make sure we have a character
        if glyph is not None and glyph.unicode is not None and glyph.unicode < 0xFFFF:

            # get character for glyph
            t = unichr(glyph.unicode)

            # draw reference glyph
            font("Georgia", 20)
            stroke(None)
            fill(*color)
            text(t, (glyph.width + 10, 10))

# turn tool on
DrawReferenceGlyph()
