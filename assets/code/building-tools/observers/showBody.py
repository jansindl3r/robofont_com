from vanilla import *
from defconAppKit.windows.baseWindow import BaseWindowController
from mojo.events import addObserver, removeObserver
from mojo.UI import UpdateCurrentGlyphView, getDefault
from mojo.drawingTools import *

class DrawBox(BaseWindowController):

    # read some user settings for use later
    color = getDefault("glyphViewMarginColor")
    height = getDefault("glyphViewDefaultHeight") / 2.0
    useItalicAngle = getDefault("glyphViewShouldUseItalicAngleForDisplay")

    def __init__(self):
        self.w = FloatingWindow((123, 40), "body", minSize=(123, 200))

        # a checkbox to turn the tool on/off
        self.w.draw = CheckBox((10, 10, -10, 20), 'show', value=True, callback=self.updateViewCallback)

        addObserver(self, "drawBox", "drawBackground")
        self.setUpBaseWindowBehavior()
        self.w.open()

    def windowCloseCallback(self, sender):
        removeObserver(self, 'drawBackground')
        super(DrawBox, self).windowCloseCallback(sender)

    def updateViewCallback(self, sender):
        UpdateCurrentGlyphView()

    def drawBox(self, info):
        # get settings
        if not self.w.draw.get():
            return

        # get glyph
        glyph = info["glyph"]
        if glyph is None:
            return

        # get font
        font = glyph.getParent()
        if font is None:
            return

        # get box top/bottom
        descender = font.info.descender
        upm = font.info.unitsPerEm

        save()

        # use italic angle
        if self.useItalicAngle:
            angle = font.info.italicAngle
            if angle is not None:
                skew(-angle, 0)

        # use slant offset
        slantOffset = font.lib.get("com.typemytype.robofont.italicSlantOffset", 0)

        # draw top/bottom boxes
        fill(*self.color)
        rect(slantOffset, descender, glyph.width, -self.height)
        rect(slantOffset, descender + upm, glyph.width, self.height)

        restore()

        # done
        UpdateCurrentGlyphView()


# open tool
DrawBox()