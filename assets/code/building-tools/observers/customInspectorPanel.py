from vanilla import *
from mojo.events import addObserver

class CustomInspectorExample(object):

    def __init__(self):
        # subscribe to the moment when an inspector will be shown
        addObserver(self, "inspectorWindowWillShowDescriptions", "inspectorWindowWillShowDescriptions")
        # subscribe to the moment when a glyph window will be shown
        addObserver(self, "glyphWindowWillShowToolbarItems", "glyphWindowWillShowToolbarItems")
        # subscribe to the moment when a font window will be shown
        addObserver(self, "fontWindowWillShowToolbarItems", "fontWindowWillShowToolbarItems")
        # keep a reference of the view inserted in the inspector
        self.editor = TextEditor((10, 10, -10, -0))

    def inspectorWindowWillShowDescriptions(self, notification):
        # create an inspector item
        item = dict(label="My Custom View", view=self.editor)
        # insert or append the item to the list of inspector panes
        notification["descriptions"].insert(0, item)

    def glyphWindowWillShowToolbarItems(self, notification):
        # create a toolbar item
        item = dict(itemIdentifier="customGlyphToolbar", label="Do It", callback=self.doIt, imageNamed="toolbarRun")
        # insert or append the item to the list of glyph window toolbar items
        notification["toolbarItems"].insert(-2, item)

    def fontWindowWillShowToolbarItems(self, notification):
        # create a toolbar item
        item = dict(itemIdentifier="customFontToolbar", label="Do It", callback=self.doIt, imageNamed="toolbarRun")
        # insert or append the item to the list of font window toolbar items
        notification["toolbarItems"].insert(2, item)

    def doIt(self, sender):
        print("do it")

CustomInspectorExample()