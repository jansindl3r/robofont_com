from mojo.UI import CurrentSpaceCenter
from vanilla import Button

class AddButtonToSpaceCenter:

    def __init__(self):
        buttonWidth = 40
        padding = 10

        # get the current space center
        sp = CurrentSpaceCenter()

        # shift pre input to the right
        x, y, w, h = sp.top.glyphLinePreInput.getPosSize()
        x += buttonWidth + padding
        sp.top.glyphLinePreInput.setPosSize((x, y, w, h))

        # shift text input to the right
        x, y, w, h = sp.top.glyphLineInput.getPosSize()
        x += buttonWidth + padding
        sp.top.glyphLineInput.setPosSize((x, y, w, h))

        # add a button
        x = y = padding
        sp.myButton = Button((x, y, buttonWidth, 22), "OK", callback=self.buttonCallback)

    def buttonCallback(self, sender):
        print("Do something")

AddButtonToSpaceCenter()