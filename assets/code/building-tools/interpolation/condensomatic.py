'''
Generate condensed glyphs from a Regular and a Bold.

Calculates a scaling factor from the Regular and Bold stem widths.

Uses interpolation and horizontal scaling to create condensed glyphs.

'''

# get Condensed, Regular and Bold fonts
condensedFont = CurrentFont()
regularFont = AllFonts().getFontsByStyleName('Roman')[0]
boldFont = AllFonts().getFontsByStyleName('Bold')[0]

# measure the Regular and Bold stem widths
regularStem = 95
boldStem = 140

# condensing factor
condenseFactor = 0.85

# calculate scale factor from stem widths
xFactor = float(regularStem) / (regularStem + condenseFactor * (boldStem - regularStem))

# interpolate selected glyphs
for glyphName in condensedFont.templateSelectedGlyphNames:

    # get Condensed, Regular and Bold glyphs
    condensedGlyph = condensedFont.newGlyph(glyphName, clear=True)
    regularGlyph = regularFont[glyphName]
    boldGlyph = boldFont[glyphName]

    # interpolate Regular and Bold
    condensedGlyph.clear()
    condensedGlyph.interpolate((condenseFactor, 0), regularGlyph, boldGlyph)

    # scale glyph horizontally
    condensedGlyph.scaleBy((xFactor, 1))

    # calculate glyph margins
    condensedGlyph.leftMargin = (regularGlyph.leftMargin + boldGlyph.leftMargin) * 0.5 * (1.0 - condenseFactor)
    condensedGlyph.rightMargin = (regularGlyph.rightMargin + boldGlyph.rightMargin) * 0.5 * (1.0 - condenseFactor)

    # done!
    condensedGlyph.changed()