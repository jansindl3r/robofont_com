from string import ascii_lowercase, ascii_uppercase, digits, punctuation

f = CurrentFont()
fontName = '%s-%s' % (f.info.familyName, f.info.styleName)

# install the font locally
# f.testInstall()

# settings
MARGIN = 20
FONTSIZE = 14
LINEHEIGHT = 1.1 * FONTSIZE

# make some test strings
testStrings = [
    ''.join(['%s%s%s\n' % (c, c.join(list(ascii_lowercase)), c) for c in ascii_lowercase]),
    ''.join(['%s%s%s\n' % (c, c.join(list(ascii_uppercase)), c) for c in ascii_uppercase]),
    ''.join(['%s%s%s\n' % (c, c.join(list(punctuation)), c) for c in ascii_lowercase]),
    ''.join(['%s%s%s\n' % (c, c.join(list(digits)), c) for c in digits]), 
]

# set text with test strings
for txt in testStrings:

    # create new page
    newPage('A4Landscape')

    # set font
    if fontName in installedFonts():
        font(fontName)
    else:
        print('font %s not installed' % fontName)

    # calculate pos/size
    x = y = MARGIN
    w = width() - MARGIN * 2
    h = height() - MARGIN * 2

    # set text sample
    fontSize(FONTSIZE)
    lineHeight(LINEHEIGHT)
    textBox(txt, (x, y, w, h))