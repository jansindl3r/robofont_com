'''Import glyphs from a second font into a layer of the current font.'''

# get the current font
font1 = CurrentFont()

# open a second font (without the UI)
font2 = OpenFont(showInterface=False)

# create target layer name
layerName = "%s %s" % (font2.info.familyName, font2.info.styleName)

# loop over all glyphs in the second font
for glyph in font2:

    # if the glyph does not exist in the current font, create it
    if glyph.name not in font1:
        f.newGlyph(glyph.name)

    # get the layer glyph
    layerGlyph = font1[glyph.name].getLayer(layerName)

    # get a point pen for the layer glyph
    pen = layerGlyph.getPointPen()

    # draw the glyph into the layer glyph
    glyph.drawPoints(pen)

# done!