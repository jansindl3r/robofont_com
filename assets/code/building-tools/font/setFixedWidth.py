'''Set a fixed width to all glyphs in the current font.'''

# get the current font
font = CurrentFont()

# iterate over all glyphs in the font
for glyph in font:

    # set glyph width
    glyph.width = 400