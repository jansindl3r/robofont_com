'''Add anchors with a given name and height in the selected glyphs.'''

# get the current font
font = CurrentFont()

# define anchor name and y-position
anchorName = 'top'
anchorY = 800

# loop over selected glyphs
for glyph in font.selectedGlyphs:

    # get the glyph bounds (aka glyph box)
    # bounds is either a tuple of (left, bottom, right, top)
    # or None if the glyph has no contours
    bounds = glyph.bounds

    # skip empty glyphs
    if bounds is None:
        continue

    # get the horizontal center
    L, B, R, T = bounds
    anchorX = L + (R - L) * 0.5

    # add anchor to glyph
    glyph.prepareUndo('add anchor')
    glyph.appendAnchor(anchorName, (anchorX, anchorY))
    glyph.performUndo()