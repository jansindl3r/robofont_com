---
layout: page
title: Documentation
menuOrder: 1
tree:
  - introduction
  - workspace
  - extensions
  - how-tos
  - building-tools
---

{% comment %}
internallink not working correctly on this page…
using markdown links instead
{% endcomment %}

[Introduction](introduction)
: An abstract description of RoboFont. The ideas and motivations behind it.

[Workspace](workspace)
: What RoboFont looks like. A description of its menus, windows, panels, dialogs etc.

[Extensions](extensions)
: How to install and use RoboFont extensions.

[How-Tos](how-tos)
: How to accomplish specific tasks with RoboFont.

[Building Tools](building-tools)
: Everything you need to know to develop your own RoboFont-based tools.

> - {% internallink "RF3-changes" %}
> - {% internallink "known-bugs" %}
{: .seealso }

> Use your left / right arrow keys to navigate to the previous / next pages in the documentation tree.
{: .tip }

> Found an error? Ideas for improvements? Please use the [forum] to get in touch!
{: .note }

[forum]: http://forum.robofont.com/category/12/documentation
