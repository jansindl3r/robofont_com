$(document).ready(function () {
    blogapi = "https://forum.robofont.com/api/category/16/announcements/"
    $.getJSON(blogapi, function(result){
        dest = $("#news-titles")
        result.topics.reverse()
        $.each(result.topics, function(i, topic) {
            dest.append(
                $("<h1/>").append(
                    $("<a/>", {href: "http://forum.robofont.com/topic/" + topic.slug }).append(
                        topic.title)
                )
            )
        })
    })
})