import json
import ftplib
import os

try:
    # this module is ignored in git...
    from ftpSettings import *
except:    
    ftpLogin = ftpPassword = None

if ftpLogin and ftpPassword:
    print("\textracting downloads...")
    
    ftpHost = "207.38.94.44" 
    ftpPath = "webapps/static/robofont/versionHistory"

    session = ftplib.FTP(ftpHost, ftpLogin, ftpPassword)
    session.cwd(ftpPath)

    try:
        files = session.nlst()
    except Exception:
        files = []

    session.quit()

    data = dict(release=[], beta=[])

    for fileName in reversed(sorted(files)):
        if fileName.endswith(".dmg"):
            parsed = fileName.replace(".dmg", "").replace("_beta", "")
            _, version, build = parsed.split("_")
            year = build[0:2]
            month = build[2:4] 
            day = build[4:6]
            hour = build[6:8]
            minute = build[8:10]
            if "b" in version:
                dest = data["beta"]
            else:
                dest = data["release"]
            dest.append(dict(
                build=build,
                date="20%s-%s-%s %s:%s" % (year, month, day, hour, minute),
                version=version,
                url = "http://static.typemytype.com/robofont/versionHistory/%s" % fileName,            
            ))
    
    dest = os.path.join(os.path.dirname(__file__), "..", "_data", "downloads.json")
    
    with open(dest, "w") as f:
        f.write(json.dumps(data))
    print("...done.\n")
        