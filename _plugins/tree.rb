# from: https://nathangrigg.com/2013/11/jekyll-plugin-look-up-page-by-url
require_relative "helpers"


module Jekyll
  class TreeBuilder < Liquid::Tag

    include HelpersTools

    def initialize(tag_name, text, tokens)
      super
      text = text.strip()
      parts = text.split(" ")
      @rawRootURL = parts[0]
      @levels = 100000
      parts[1..-1].each do |item|
        args = item.strip().split("=")
        if args[0] == "levels"
          @levels = Integer(args[1])
        end
      end
    end

    def renderTreeItem(context, treeItem)
      if @currentLevel >= @levels
        return
      end
      if treeItem.end_with? "*"
        treeRoot = treeItem.sub("*", "")
        page_hash(context).sort.map.each do |key, value|
          if key.include? treeRoot and key != treeItem
            @currentLevel += 1
            renderTreeItem(context, key)
            @currentLevel -= 1
          end
        end
      else
        page = page_hash(context)[treeItem]
        unless page.nil?
          if page.data.has_key?("draft-hidden") and page["draft-hidden"]
            pinkWarning("Page hidden: '#{treeItem}'")
          else
            fullPageURL = File.join(@baseurl, page.url.gsub(TrailingIndex, ''))
            @output += "<li"
            if @currentLevel >= 1
              unless @currentSection.nil?
                if treeItem.include? @currentSection
                  @output += " class=\"current-section\""
                else
                  @output += " class=\"hide-section\""
                end
              end
            end
            @output += ">"
            @output += "<a "
            if page.url == context.registers[:page]["url"]
              @output += "class='current' "
            end
            @output += "href='#{fullPageURL}'>"
            if page.data.has_key?("treeTitle")
              @output += page["treeTitle"]
            else
              @output += page["title"]
            end
            @output += "</a>"
            if page.data.has_key?("tree")
              @output += "<ul"
              if page.data.has_key?("treeCanHide") and page["treeCanHide"]
                @output += " class=\"treecanhide\""
              end
              @output += ">"
              page["tree"].each do |subTreeItem|
                subTreeItem = File.join(page.url.gsub(TrailingIndex, ''), subTreeItem)
                subTreeItem = File.absolute_path(subTreeItem)
                @currentLevel += 1
                renderTreeItem(context, subTreeItem)
                @currentLevel -= 1
              end
              @output += "</ul>"
            end
            @output += "</li>"
          end
        else
           pinkWarning("Page not found: '#{treeItem}' in #{context.registers[:page]["url"]}")
        end
      end
    end

    def render(context)
      url = get_value(context, @rawRootURL)
      @currentPage = context.registers[:page]
      @currentSection = @currentPage["url"].split("/")[2]
      unless @currentSection.nil?
        @currentSection = "/" + @currentSection + "/"
      end
      if url.nil?
        pinkWarning("Cannot parse '#{@rawRootURL}' in #{@currentPage["url"]}")
        return
      end
      @rootURL = File.absolute_path(url.gsub(TrailingIndex, ''))
      @currentLevel = 0
      @output = "<ul>"
      @baseurl = context.registers[:site].baseurl
      if @rootURL.end_with? "*"
        treeRoot = @rootURL.sub("*", "")
        page_hash(context).sort.map.each do |key, value|
          if key.include? treeRoot and key != @rootURL
            renderTreeItem(context, key)
          end
        end
      else
        page = page_hash(context)[@rootURL]
        unless page.nil?
          if page.data.has_key?("tree")
            page["tree"].each do |treeItem|
              treeItem = File.join(@rootURL, treeItem)
              treeItem = File.absolute_path(treeItem)
              renderTreeItem(context, treeItem)
            end
          end
        else
          pinkWarning("Page not found: '#{@rootURL}' in #{context.registers[:page]["url"]}")
        end
      end
      @output += "</ul>"
      @output
    end
  end
end

Liquid::Template.register_tag('tree', Jekyll::TreeBuilder)