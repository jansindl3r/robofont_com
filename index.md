---
layout: default
title: RoboFont
hideTitle: true
excludeSearch: true
excludeNavigation: true
pageId: home-page
slideShows:
  intro:
    - image: workspace/window-modes_single.png
    - image: workspace/font-overview.png
    - image: workspace/glyph-editor.png
    - image: workspace/space-center.png
    - image: workspace/scripting-window.png
    - image: workspace/features-editor.png
    - image: workspace/preferences_glyph-view-appearance.png
  intro-extensions:
    - image: extensions/overview/outliner.png
    - image: extensions/overview/italic-bowtie.png
    - image: extensions/overview/word-o-mat.png
    - image: extensions/overview/ramsay-st.png
    - image: extensions/overview/glifViewer.png
    - image: extensions/overview/GlyphBrowser.png
    - image: extensions/overview/GlyphConstruction.png
    - image: extensions/overview/GlyphNanny.png
    - image: extensions/overview/OverlayUFOs.png
    - image: extensions/overview/Slanter.png
  autoPlay: true
  height: 600
---

{% comment %}
dirty hack to reduce the height of the 2nd slideshow
{% endcomment %}
<style>
.slides:nth-of-type(2) { height: 420px; }
</style>

{% comment %}
slideshow : windows
{% endcomment %}
{% include slideshows items=page.slideShows.intro %}

{% comment %}
splash 1
{% endcomment %}
<div class="center splash-subtitle">
The <span id='adjective'></span> UFO font editor.
</div>

<script type="text/javascript">
    var adjectives = [
        'missing',
        'heavy-duty',
        'poetic',
        'extensible',
        'avant-garde',
        'professional',
        'interstellar',
        'pythonic',
        'water-proof',
        'elegant',
        'industrial',
    ];
    window.onload = function() {
        document.getElementById("adjective").innerHTML = adjectives[Math.floor(Math.random() * adjectives.length)];
    }
</script>

{% comment %}
download & buy
{% endcomment %}
<div class="center">
    <a class="buy-button" href="http://static.typemytype.com/robofont/RoboFont.dmg">download</a>
    <a class="buy-button" href="https://sites.fastspring.com/typemytype/instant/robofont3">buy</a>
</div>

{::options parse_block_html="true" /}

{% comment %}
columns 1: intro text
{% endcomment %}
<div class="row item">

<div class="col">
Written from scratch in Python with scalability in mind.

A fully featured font editor with all the tools required for drawing typefaces.
</div>

<div class="col">
Provides full scripting access to objects and interface.

A platform for building your own tools and extensions, and much more…!
</div>

</div>

{% comment %}
splash icons
{% endcomment %}
<div class="row item navigation-splash">
<a class="documentation" href="{{ site.baseurl }}/documentation/introduction"><span class="icon">D</span>Introduction</a>
<a class="documentation" href="{{ site.baseurl }}/documentation/workspace"><span class="icon">W</span>Workspace</a>
<a class="how-tos" href="{{ site.baseurl }}/documentation/how-tos"><span class="icon">T</span>How-To’s</a>
<a class="building-tools" href="{{ site.baseurl }}/documentation/building-tools"><span class="icon">A</span>Building Tools</a>
<a class="extensions" href="{{ site.baseurl }}/documentation/extensions/"><span class="icon">P</span>Extensions</a>
<a class="extensions" href="http://robofontmechanic.com"><span class="icon">M</span>Mechanic</a>
<a class="store" href="http://extensionstore.robofont.com"><span class="icon">S</span>Extension Store</a>
<a class="education" href="http://education.robofont.com"><span class="icon">E</span>Education</a>
<a class="forum" href="http://forum.robofont.com"><span class="icon">F</span>Forum</a>
<!-- <a class="news" href="http://forum.robofont.com/category/16/announcements/"><span class="icon">N</span>Announcements</a> -->
</div>

{% comment %}
columns 2: technical specs + educational
{% endcomment %}
<div class="row item">

<div class="col">
## Technical specifications

- requires macOS {{site.data.versions.minimumSystem}} or higher
- uses UFO3 as native font format
- supports Python {{site.data.versions.python}} out of the box

[read more…][Technical specification]
</div>

<div class="col">
## Educational licenses

Teachers can request a free 1-year trial license for their students, or subscribe to the Student License Service.

[read more…][Educational licensing]
</div>

</div>

{% comment %}
splash 2
{% endcomment %}
<div class="center splash-subtitle">
The tools you choose influence your creative process!
</div>

<br/>

{% comment %}
slideshow: extensions
{% endcomment %}
{% include slideshows items=page.slideShows.intro-extensions %}

{% comment %}
columns 3: extensions
{% endcomment %}
<div class="row item">

<div class="col">
## Open-source extensions

Dozens of open-source extensions by multiple developers are available on GitHub and via Mechanic.

[read more…][Extensions]
</div>

<div class="col">
## Commercial Extensions

Several other extensions by certified developers are available for a fee in the Extension Store.

[read more…][Extension Store]
</div>

</div>

{::options parse_block_html="false" /}

[Technical specification]: {{site.baseurl}}/technical-specification
[Student licenses]: {{site.baseurl}}/student-license
[Educational licensing]: http://education.robofont.com
[Contact]: {{site.baseurl}}/contact
[License Agreement]: {{site.baseurl}}/eula
[Version History]: {{site.baseurl}}/version-history
[Extensions]: {{site.baseurl}}/documentation/extensions/
[Extension Store]: http://extensionstore.robofont.com

<br/>

<script src="{{site.baseurl}}/js/news.js"></script>

<div class="posts news">
    <h1><a href="http://forum.robofont.com/category/16/announcements/">Announcements:</a></h1>
    <div id="news-titles"></div>
</div>
