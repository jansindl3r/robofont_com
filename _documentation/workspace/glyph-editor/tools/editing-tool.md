---
layout: page
title: Editing Tool
tags:
  - contours
level: beginner
---

* Table of Contents
{:toc}

{% image workspace/glyph-editor_editing-tool.png %}

A tool to edit visual glyph data: contours, anchors, components, guidelines, etc.

## Actions

### Point Selection

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>click (no selection)</td>
    <td>Deselect all.</td>
  </tr>
  <tr>
    <td>click + drag (no selection)</td>
    <td>Select all points inside the selection marquee.</td>
  </tr>
  <tr>
    <td>click on a point</td>
    <td>Select the point.</td>
  </tr>
  <tr>
    <td>⇧ + click</td>
    <td>Add or remove from current selection.</td>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Move selected points.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag</td>
    <td>Move selected points on x, y or 45° axis.</td>
  </tr>
  <tr>
    <td>⌥ + click + drag</td>
    <td>The point will be turned non-smooth and only the on-curve point is moved.</td>
  </tr>
  <tr>
    <td>⌃ + click + drag</td>
    <td>Copies the selection while dragging.</td>
  </tr>
  <tr>
    <td>⌥ + click + drag (single BCP selection)</td>
    <td>BCP becomes non-smooth.</td>
  </tr>
  <tr>
    <td>⌃ + ⌥ + click + drag (single BCP selection)</td>
    <td>BCP becomes smooth, and the BCP value is mirrored on the other BCP (if there is one).</td>
  </tr>
  <tr>
    <td>⌘ + drag smooth point</td>
    <td>
      <dl>
        <dt>If selection is on-curve point:</dt>
        <dd>Point will be moved between the handles.</dd>
        <dt>If selection is off-curve point:</dt>
        <dd>Point will be moved in the same direction as the handle.</dd>
      </dl>
    </td>
  </tr>
  <tr>
    <td>join / close contour</td>
    <td>If a contour is open, and first or last on-curve point is dragged on top of another first or last on-curve point, the contours are joined or closed.</td>
  </tr>
  <tr>
    <td>tab</td>
    <td>Jump to the next on-curve point.</td>
  </tr>
</table>

> Use ⇪ (Caps Lock) + ⌘ + arrow keys to move a smooth point between its handles using the keyboard instead of the mouse. See [this post](http://forum.robofont.com/topic/572/moving-on-curve-independent-of-off-curves-using-arrows-maintaining-smooth) in the RoboFont Forum.
{: .tip }

### Segment Selection

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>click</td>
    <td>Select segment.</td>
  </tr>
  <tr>
    <td>⇧ + click</td>
    <td>Add segment to selection.</td>
  </tr>
  <tr>
    <td>click + drag (single segment selection)</td>
    <td>Move the selected segments around.</td>
  </tr>
  <tr>
    <td>⌥ + click + drag (single segment selection)</td>
    <td>
      <dl>
        <dt>For line segment:</dt>
        <dd>Create curve segment (adds two BCPs).</dd>
        <dt>For curve segment:</dt>
        <dd>Drag the handles around. (This will un-smooth on-curve points.)</dd>
      </dl>
    </td>
  </tr>
  <tr>
    <td>⌘ + click drag (single segment selection)</td>
    <td>
      <dl>
        <dt>For line segments:</dt>
        <dd>Move the selected segment around.</dd>
        <dt>For curve segments:</dt>
        <dd>BCPs follow the handles direction.</dd>
      </dl>
    </td>
  </tr>
  <tr>
    <td>⇧ + click + drag (single segment selection)</td>
    <td>All points are constrained to x, y or 45°. (Works also while ⌥ and/or ⌘ are down.)</td>
  </tr>
  <tr>
    <td>⌃ + click + drag</td>
    <td>Copy the selected segment while dragging.</td>
  </tr>
  <tr>
    <td>⌘ + ⌥ (no selection)</td>
    <td>Add point to segment.</td>
  </tr>
</table>

### Contour Selection

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>double click</td>
    <td>Select contour.</td>
  </tr>
  <tr>
    <td>click + drag (in/on selection)</td>
    <td>Move the selection.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag (in/on selection)</td>
    <td>Move the selection with x, y or 45° constrain.</td>
  </tr>
</table>

### Component Selection

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>click</td>
    <td>Select the component.</td>
  </tr>
  <tr>
    <td>⇧ + click</td>
    <td>Toggles the component selection.</td>
  </tr>
  <tr>
    <td>drag</td>
    <td>Move the components around.</td>
  </tr>
  <tr>
    <td>⇧ + drag</td>
    <td>Move the selection with x, y or 45° constrain.</td>
  </tr>
  <tr>
    <td>triple click (in the component)</td>
    <td>Go to the base glyph for editing.</td>
  </tr>
</table>

### Copy & Paste

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>copy</td>
    <td>
      <dl>
        <dt>in Glyph View:</dt>
        <dd>Copy the current glyph’s selection to clipboard.</dd>
        <dt>in Font Overview:</dt>
        <dd>Copy the selected glyph to clipboard.</dd>
        <dt>in Adobe Illustrator™:</dt>
        <dd>Copy selection to clipboard.</dd>
      </dl>
    </td>
  </tr>
  <tr>
    <td>paste</td>
    <td>Paste clipboard contents into the current glyph.</td>
  </tr>
</table>

## Contextual Menus

Contextual menus can be opened using a right-click. Depending on the current selection and on where you click, a different menu will be shown.

### No selection

{% image workspace/glyph-editor_editing-tool_contextual-menu.png %}

<table>
  <thead>
    <tr>
      <th width="35%">action</th>
      <th width="65%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Add Component</td>
      <td>Open an Add Component sheet.</td>
    </tr>
    <tr>
      <td>Add Anchors</td>
      <td>Open an Add Anchor sheet.</td>
    </tr>
    <tr>
      <td>Reverse</td>
      <td>Reverse the whole glyph.</td>
    </tr>
    <tr>
      <td>Remove Overlap</td>
      <td>Remove overlaps in the whole glyph.</td>
    </tr>
    <tr>
      <td>Auto Contour order</td>
      <td>Try to order the contours.</td>
    </tr>
    <tr>
      <td>Add Extreme Points</td>
      <td>Add extreme points to selected contours.</td>
    </tr>
    <tr>
      <td>Corrent Directions (PS)</td>
      <td>Correct contour directions (PostScript).</td>
    </tr>
    <tr>
      <td>⌥ + Corrent Directions (TT)</td>
      <td>Correct contour directions (TrueType).</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Lock Guides</td>
      <td>Lock all guides (global setting).</td>
    </tr>
    <tr>
      <td>Lock Images</td>
      <td>Lock all images (global setting).</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Copy To Layer</td>
      <td>List all available layers to copy to.</td>
    </tr>
    <tr>
      <td>⌥ + Copy To Layer</td>
      <td>List all available layers to swap with.</td>
    </tr>
  </tbody>
</table>

### Contour selection

<table>
  <thead>
    <tr>
      <th width="35%">action</th>
      <th width="65%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Reverse</td>
      <td>Reverse selected contours.</td>
    </tr>
    <tr>
      <td>Remove Overlap</td>
      <td>Remove overlaps in selected contours.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Labels</td>
      <td>Add labels to selected points.</td>
    </tr>
  </tbody>
</table>

### Single point

{% image workspace/glyph-editor_editing-tool_contextual-menu_point.png %}

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>Break Contour</td>
    <td>Breaks contour at the selected point.</td>
  </tr>
  <tr>
    <td>Reverse</td>
    <td>Reverse the whole glyph.</td>
  </tr>
  <tr>
    <td>Set Start Point</td>
    <td>Set the selected point as starting point.</td>
  </tr>
</table>

### Glyph with components

{% image workspace/glyph-editor_editing-tool_contextual-menu_component.png %}

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>Go to</td>
    <td>Jump to a component’s base glyph.</td>
  </tr>
  <tr>
    <td>⌥ + Go to</td>
    <td>Decompose component selected in contextual menu.</td>
  </tr>
  <tr>
    <td>Decompose selected</td>
    <td>Decompose selected components.</td>
  </tr>
  <tr>
    <td>Decompose All</td>
    <td>Decompose all components.</td>
  </tr>
</table>

### Anchor

{% image workspace/glyph-editor_editing-tool_contextual-menu_anchor.png %}

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>Name</td>
    <td>Set a name for the selected anchor.</td>
  </tr>
</table>

### Guideline

See {% internallink "glyph-editor/guidelines" %}.

### Image

See {% internallink "glyph-editor/image" %}.
