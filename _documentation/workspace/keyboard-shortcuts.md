---
layout: page
title: Keyboard Shortcuts
level: beginner
---

* Table of Contents
{:toc}

## File

<table>
  <tr>
    <th width='25%'>keys</th>
    <th width='75%'>action</th>
  </tr>
  <tr>
    <td>⌘ N</td>
    <td>Create a new font.</td>
  </tr>
  <tr>
    <td>⌘ O</td>
    <td>Open an existing document (font or script).</td>
  </tr>
  <tr>
    <td>⌘ S</td>
    <td>Save the document.</td>
  </tr>
  <tr>
    <td>⇧ ⌘ S</td>
    <td>Save document under another name.</td>
  </tr>
  <tr>
    <td>⌘ W</td>
    <td>Close the current document.</td>
  </tr>
  <tr>
    <td>⌥ ⌘ G</td>
    <td>Generate the current font.</td>
  </tr>
</table>

## Edit

<table>
  <tr>
    <th width='25%'>keys</th>
    <th width='75%'>action</th>
  </tr>
  <tr>
    <td>⌘ Z</td>
    <td>Undo the last action.</td>
  </tr>
  <tr>
    <td>⇧ ⌘ Z</td>
    <td>Redo the last action.</td>
  </tr>
  <tr>
    <td>⌘ X</td>
    <td>Cut the current selection to the clipboard.</td>
  </tr>
  <tr>
    <td>⌘ C</td>
    <td>Copy the current selection to the clipboard.</td>
  </tr>
  <tr>
    <td>⌥ ⌘ C</td>
    <td>Copy the selected glyph to the clipboard as a component.</td>
  </tr>
  <tr>
    <td>⌘ V</td>
    <td>Paste the clipboard contents into the current document.</td>
  </tr>
  <tr>
    <td>⌘ A</td>
    <td>Select all contours.</td>
  </tr>
  <tr>
    <td>⌥ ⌘ A</td>
    <td>Select all objects in the glyph.</td>
  </tr>
  <tr>
    <td>⇧ ⌘ A</td>
    <td>Deselect.</td>
  </tr>
  <tr>
    <td>⌘ D</td>
    <td>Apply the last transformation again.</td>
  </tr>
</table>

## Font

<table>
  <tr>
    <th width='25%'>keys</th>
    <th width='75%'>action</th>
  </tr>
  <tr>
    <td>⌃ ⌘ G</td>
    <td>Show <a href="../../how-tos/adding-and-removing-glyphs">Adding Glyphs sheet</a>.</td>
  </tr>
  <tr>
    <td>⌥ ⌘ I</td>
    <td>Show <a href="../font-info-sheet">Font Info sheet</a>.</td>
  </tr>
  <tr>
    <td>⌘ F</td>
    <td>Find glyph.</td>
  </tr>
</table>

## Glyph

<table>
  <tr>
    <th width='25%'>keys</th>
    <th width='75%'>action</th>
  </tr>
  <tr>
    <td>⌘ )</td>
    <td>Go to next glyph.</td>
  </tr>
  <tr>
    <td>⌘ (</td>
    <td>Go to previous glyph.</td>
  </tr>
  <tr>
    <td>⌘ 1</td>
    <td>Selects the <a href="../glyph-editor/tools/editing-tool">Editing Tool</a>.</td>
  </tr>
  <tr>
    <td>⌘ 2</td>
    <td>Selects the <a href="../glyph-editor/tools/bezier-tool">Bezier Tool</a>.</td>
  </tr>
  <tr>
    <td>⌘ 3</td>
    <td>Selects the <a href="../glyph-editor/tools/slice-tool">Slice Tool</a>.</td>
  </tr>
  <tr>
    <td>⌘ T</td>
    <td>Activate Transform mode.</td>
  </tr>
  <tr>
    <td>⌥ ⌘ ↑</td>
    <td>Jump to next layer (up).</td>
  </tr>
  <tr>
    <td>⌥ ⌘ ↓</td>
    <td>Jump to previous layer (down).</td>
  </tr>
  <tr>
    <td>⌥ ⇧ N</td>
    <td>Create a new layer.</td>
  </tr>
</table>

## Window

<table>
  <tr>
    <th width='25%'>keys</th>
    <th width='75%'>action</th>
  </tr>
  <tr>
    <td>⇧ ⌘ T</td>
    <td>Show/hide Tab bar.</td>
  </tr>
  <tr>
    <td>⌘ I</td>
    <td>Open the <a href="../inspector">Inspector</a>.</td>
  </tr>
  <tr>
    <td>⌥ ⌘ S</td>
    <td>Open the <a href="../space-center">Space Center</a>.</td>
  </tr>
  <tr>
    <td>⌘ M</td>
    <td>Minize window.</td>
  </tr>
  <tr>
    <td>⌃ ⌘ F</td>
    <td>Enter Full Screen mode.</td>
  </tr>
  <tr>
    <td>⌘ +</td>
    <td>Zoom in.</td>
  </tr>
  <tr>
    <td>⌘ -</td>
    <td>Zoom out.</td>
  </tr>
  <tr>
    <td>⌘ 0</td>
    <td>Zoom to fit.</td>
  </tr>
  <tr>
    <td>⌘ J</td>
    <td>Jump to glyph.</td>
  </tr>
</table>

## Scripting Window

<table>
  <tr>
    <th width='25%'>keys</th>
    <th width='75%'>action</th>
  </tr>
  <tr>
    <td>⌥ ⌘ R</td>
    <td>Open a <a href="../scripting-window">Scripting Window</a>.</td>
  </tr>
  <tr>
    <td>⌥ ⌘ O</td>
    <td>Open the <a href="../output-window">Output Window</a>.</td>
  </tr>
  <tr>
    <td>⌘ R</td>
    <td>Execute script.</td>
  </tr>
  <tr>
    <td>⌘ ]</td>
    <td>Indent selected code.</td>
  </tr>
  <tr>
    <td>⌘ [</td>
    <td>Dedent selected code.</td>
  </tr>
  <tr>
    <td>⌘ }</td>
    <td>Comment selected code.</td>
  </tr>
  <tr>
    <td>⌘ {</td>
    <td>Uncomment selected code.</td>
  </tr>
  <tr>
    <td>⌥ ⇧ ⌘ W</td>
    <td>Wrap words.</td>
  </tr>
  <tr>
    <td>⌘ J</td>
    <td>Jump to line number.</td>
  </tr>
  <tr>
    <td>⌥ ⌘ J</td>
    <td>Jump to definition match.</td>
  </tr>
  <tr>
    <td>⇧ ⌘ J</td>
    <td>Jump back to previous definition match.</td>
  </tr>
</table>

## Miscellaneous

<table>
  <tr>
    <th width='25%'>keys</th>
    <th width='75%'>action</th>
  </tr>
  <tr>
    <td>⌘ ,</td>
    <td>Open the <a href="../preferences-window">Preferences Window</a>.</td>
  </tr>
  <tr>
    <td>⌥ ⌘ ,</td>
    <td>Open the <a href="../preferences-editor">Preferences Editor</a>.</td>
  </tr>
  <tr>
    <td>⌃ ⌥ ⌘ O</td>
    <td>Go to the RoboFont log file in Finder.</td>
  </tr>
</table>
