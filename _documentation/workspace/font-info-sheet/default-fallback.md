---
layout: page
title: Font Info default and fallback values
tags:
  - mastering
excludeNavigation: true
---

* Table of Contents
{:toc}

The tables below show the default and fallback values for all fields in the {% internallink "font-info-sheet" %}.

> - <span class='required'>required</span>
> - <span class='recommended'>recommended</span>
{: .note }

## General font info

### Identification

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td class='required'>Family Name</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='required'>Style Name</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='recommended'>Style Map Family Name</td>
    <td><em>empty</em></td>
    <td>Preferred Family Name followed by Preferred Subfamily Name</td>
  </tr>
  <tr>
    <td>Style Map Style</td>
    <td><em>empty</em></td>
    <td>regular</td>
  </tr>
  <tr>
    <td class='recommended'>Version Major</td>
    <td><em>empty</em></td>
    <td>0</td>
  </tr>
  <tr>
    <td class='recommended'>Version Minor</td>
    <td><em>empty</em></td>
    <td>0</td>
  </tr>
</table>

### Dimensions

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td class='required'>Units Per Em</td>
    <td>1000</td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='required'>Descender</td>
    <td>-250</td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='required'>x-height</td>
    <td>500</td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='required'>Cap-height</td>
    <td>750</td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='required'>Ascender</td>
    <td>750</td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Italic Angle</td>
    <td><em>empty</em></td>
    <td>0</td>
  </tr>
</table>

> When a new font is created with Python without the UI, its vertical metrics are empty.
{: .note }

### Legal

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td class='recommended'>Copyright</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='recommended'>Trademark</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='recommended'>License</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='recommended'>License URL</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

### Parties

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td class='recommended'>Designer</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='recommended'>Designer URL</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='recommended'>Manufacturer</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='recommended'>Manufacturer URL</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

### Note

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>Note</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

## OpenType

### gasp table

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>gasp records</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

### head table

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>created</td>
    <td><em>empty</em></td>
    <td>Current date/time.</td>
  </tr>
  <tr>
    <td>lowestRecPPEM</td>
    <td><em>empty</em></td>
    <td>6</td>
  </tr>
  <tr>
    <td>flags</td>
    <td><em>empty</em></td>
    <td>0, 1</td>
  </tr>
</table>

### name table

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>Preferred Family Name</td>
    <td><em>empty</em></td>
    <td>Family Name</td>
  </tr>
  <tr>
    <td>Preferred Subfamily Name</td>
    <td><em>empty</em></td>
    <td>Style Name</td>
  </tr>
  <tr>
    <td>Compatible Full Name</td>
    <td><em>empty</em></td>
    <td>
      <p>Style Map Family Name followed by Style Map Style Name.</p>
      <p>If Style Map Style Name is <code>Regular</code>, it is not used.</p>
    </td>
  </tr>
  <tr>
    <td>WWS Family Name</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>WWS Subfamily Name</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Version</td>
    <td><em>empty</em></td>
    <td>Version Major followed by Version Minor in the form <code>0.000</code>.</td>
  </tr>
  <tr>
    <td>Unique ID</td>
    <td><em>empty</em></td>
    <td>A string in the form <code>OpenType Version;OS/2 Vendor ID;Style Map Family Name + Style Map Style Name</code></td>
  </tr>
  <tr>
    <td>Description</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td class='recommended'>Sample Text</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Name records</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

### hhea table

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>Ascender</td>
    <td><em>empty</em></td>
    <td>Units Per Em value + Descender value</td>
  </tr>
  <tr>
    <td>Descender</td>
    <td><em>empty</em></td>
    <td>Descender value</td>
  </tr>
  <tr>
    <td>LineGap</td>
    <td><em>empty</em></td>
    <td>200</td>
  </tr>
  <tr>
    <td>caretSlopeRise</td>
    <td><em>empty</em></td>
    <td>1</td>
  </tr>
  <tr>
    <td>caretSlopeRun</td>
    <td><em>empty</em></td>
    <td>0</td>
  </tr>
  <tr>
    <td>caretOffset</td>
    <td><em>empty</em></td>
    <td>0</td>
  </tr>
</table>

### vhea table

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>vertTypoAscender</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>vertTypoDescender</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>vertTypoLineGap</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>caretSlopeRise</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>caretSlopeRun</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>caretOffset</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

### OS/2 table

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td class='recommended'>usWidthClass</td>
    <td><em>empty</em></td>
    <td>5</td>
  </tr>
  <tr>
    <td class='recommended'>usWeightClass</td>
    <td><em>empty</em></td>
    <td>400</td>
  </tr>
  <tr>
    <td>fsSelection</td>
    <td><em>empty</em></td>
    <td><em>empty</em></td>
  </tr>
  <tr>
    <td class='recommended'>achVendID</td>
    <td><em>empty</em></td>
    <td>NONE</td>
  </tr>
  <tr>
    <td class='recommended'>fsType</td>
    <td><em>empty</em></td>
    <td>2</td>
  </tr>
  <tr>
    <td class='recommended'>ulUnicodeRange</td>
    <td><em>empty</em></td>
    <td><em>empty</em></td>
  </tr>
  <tr>
    <td class='recommended'>ulCodePageRange</td>
    <td><em>empty</em></td>
    <td><em>empty</em></td>
  </tr>
  <tr>
    <td>sTypoAscender</td>
    <td><em>empty</em></td>
    <td>Units Per Em value + Descender value</td>
  </tr>
  <tr>
    <td>sTypoDescender</td>
    <td><em>empty</em></td>
    <td>Descender value</td>
  </tr>
  <tr>
    <td class='recommended'>sTypoLineGap</td>
    <td><em>empty</em></td>
    <td>200</td>
  </tr>
  <tr>
    <td>usWinAscent</td>
    <td><em>empty</em></td>
    <td>Maximum <code>y</code> value of the font. If not available, the Ascender value.</td>
  </tr>
  <tr>
    <td>usWinDescent</td>
    <td><em>empty</em></td>
    <td>Minimum <code>y</code> value of the font. If not available, the Descender value.</td>
  </tr>
  <tr>
    <td>ySubscriptXSize</td>
    <td><em>empty</em></td>
    <td><em>Calculated by the AFDKO.</em></td>
  </tr>
  <tr>
    <td>ySubscriptYSize</td>
    <td><em>empty</em></td>
    <td><em>Calculated by the AFDKO.</em></td>
  </tr>
  <tr>
    <td>ySubscriptXOffset</td>
    <td><em>empty</em></td>
    <td><em>Calculated by the AFDKO.</em></td>
  </tr>
  <tr>
    <td>ySubscriptYOffset</td>
    <td><em>empty</em></td>
    <td><em>Calculated by the AFDKO.</em></td>
  </tr>
  <tr>
    <td>ySuperscriptXSize</td>
    <td><em>empty</em></td>
    <td><em>Calculated by the AFDKO.</em></td>
  </tr>
  <tr>
    <td>ySuperscriptYSize</td>
    <td><em>empty</em></td>
    <td><em>Calculated by the AFDKO.</em></td>
  </tr>
  <tr>
    <td>ySuperscriptXOffset</td>
    <td><em>empty</em></td>
    <td><em>Calculated by the AFDKO.</em></td>
  </tr>
  <tr>
    <td>ySuperscriptYOffset</td>
    <td><em>empty</em></td>
    <td><em>Calculated by the AFDKO.</em></td>
  </tr>
  <tr>
    <td>yStrikeoutSize</td>
    <td><em>empty</em></td>
    <td><em>Calculated by the AFDKO.</em></td>
  </tr>
  <tr>
    <td>yStrikeoutPosition</td>
    <td><em>empty</em></td>
    <td><em>Calculated by the AFDKO.</em></td>
  </tr>
  <tr>
    <td>Panose</td>
    <td><em>empty</em></td>
    <td>0, 0, 0, 0, 0, 0, 0, 0, 0, 0</td>
  </tr>
</table>

## PostScript

### Identification

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>FontName</td>
    <td><em>empty</em></td>
    <td>A combination of the OpenType Preferred Family Name and the OpenType Preferred Subfamily Name, stripping out any invalid characters.</td>
  </tr>
  <tr>
    <td>FullName</td>
    <td><em>empty</em></td>
    <td>A combination of the OpenType Preferred Family Name, a space, and the OpenType Preferred Subfamily name.</td>
  </tr>
  <tr>
    <td>WeightName</td>
    <td><em>empty</em></td>
    <td>Looks at the OS/2 <code>usWeightClass</code> value and picks the description that best matches the value.
    </td>
  </tr>
  <tr>
    <td>Unique ID Number</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

### Hinting

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>BlueValues</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>OtherBlues</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>FamilyBlues</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>FamilyOtherBlues</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>StemSnapH</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>StemSnapV</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>BlueFuzz</td>
    <td><em>empty</em></td>
    <td>0</td>
  </tr>
  <tr>
    <td>BlueShift</td>
    <td><em>empty</em></td>
    <td>7</td>
  </tr>
  <tr>
    <td>BlueScale</td>
    <td><em>empty</em></td>
    <td>.039625 (10pt at 300dpi)</td>
  </tr>
  <tr>
    <td>ForceBold</td>
    <td><em>empty</em></td>
    <td>False</td>
  </tr>
</table>

### Dimensions

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>SlantAngle</td>
    <td><em>empty</em></td>
    <td>The italic angle, if set. Otherwise, <code>0</code>.</td>
  </tr>
  <tr>
    <td>UnderlineThickness</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>UnderlinePosition</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>isFixedPitched</td>
    <td><em>empty</em></td>
    <td>Off</td>
  </tr>
  <tr>
    <td>DefaultWidthX</td>
    <td><em>empty</em></td>
    <td>200</td>
  </tr>
  <tr>
    <td>NominalWidthX</td>
    <td><em>empty</em></td>
    <td>0</td>
  </tr>
</table>

### Characters

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>Default Character</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Microsoft Character Set</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

## WOFF

### Identification

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>Major Version</td>
    <td><em>empty</em></td>
    <td>Major Version</td>
  </tr>
  <tr>
    <td>Minor Version</td>
    <td><em>empty</em></td>
    <td>Minor Version</td>
  </tr>
  <tr>
    <td>Unique ID</td>
    <td><em>empty</em></td>
    <td>openTypeNameUniqueID</td>
  </tr>
</table>

### Vendor

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>Name</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>URL</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Dir</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Class</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

### Credits

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>Name</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Role</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>URL</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Dir</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Class</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

### Description

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>URL</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Text</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Language</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Dir</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Class</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

### Legal

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>Copyright Text</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Trademark Text</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>License URL</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>License ID</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>License Text</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Licensee Name</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Licensee Dir</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Licensee Class</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>

## Miscellaneous

### FOND Data

<table>
  <tr>
    <th width='33%'>option</th>
    <th width='33%'>default</th>
    <th width='33%'>fallback</th>
  </tr>
  <tr>
    <td>Font Name</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
  <tr>
    <td>Family ID Number</td>
    <td><em>empty</em></td>
    <td><em>no fallback</em></td>
  </tr>
</table>
