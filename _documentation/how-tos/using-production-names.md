---
layout: page
title: Using production names and final names
tags:
  - character set
  - glyph names
level: beginner
draft: false
---

* Table of Contents
{:toc}

## Production names vs. final names

The demands for glyph names during the design process and at the end of the production line are different:

- During the design process, glyph names should be logical, legible and descriptive – so it’s easy to find a particular glyph while working on a font, and it’s easy to guess what a glyph looks like (even when it’s still empty).

- At the same time, glyph names in final OpenType fonts must obey certain rules in order to work correctly in all environments.

  For example, in order to enable text search in PDFs, it is necessary that glyph names follow the [Adobe Glyph List Specification]. In practice, this means that any glyph which does not have a readable name in the {% glossary AGLNF %} must be named with its unicode value: `uniXXXX`.

> - [Where and why glyph names are used (AGL)][Adobe Glyph List Specification]
{: .seealso }

[Adobe Glyph List Specification]: http://github.com/adobe-type-tools/agl-specification

## glyphOrderAndAliasDB aka GOADB

As it turns out, it is possible to get the best of both worlds:

- use ‘friendly’ glyph names during development
- switch to ‘final’ glyph names when generating OpenType fonts

> This is specially useful when working with scripts other than Latin and Greek, for which the AGL dictates ‘unfriendly’ `uniXXXX` glyph names.
{: .tip }

Under the hood, the switch from production names to final names is enabled by the `glyphOrderAndAliasDB` (GOADB) file, which can be supplied to `makeotf` when generating OpenType fonts.

## Storing GOADB data in the UFO

The GOADB data can be stored in the UFO lib, so it is passed to `makeotf` when generating a font.

### public.postscriptNames

UFO3/RF3 supports a dictionary with production names (keys) and final names (values) in the font’s `lib.plist`, under the standard key `public.postscriptnames`.

```python
f = CurrentFont()
f.lib["public.postscriptNames"] = {"A.test": "A", "Something": "C"}
f.save()
```

{% image how-tos/using-production-names_ufo.png caption="UFO file (production names)" %}

{% image how-tos/using-production-names_otf.png caption="OTF file (final names)" %}

> Notice that `.notdef` and `space` glyphs are inserted automatically by `makeotf`.
{: .note }

> - [lib.plist > public.postscriptnames (UFO3 Specification)](http://unifiedfontobject.org/versions/ufo3/lib.plist#publicpostscriptnames)
{: .seealso }

### com.typesupply.ufo2fdk.glyphOrderAndAliasDB

UFO2/RF1 stores the GOADB data as a string in the lib, under the custom lib key `com.typesupply.ufo2fdk.glyphOrderAndAliasDB`.

The string follows the original GOADB format: one glyph per line, and every line as a space-separated list with final name, production name and (optional) unicode.

```python
f = CurrentFont()

goadb = """\
A A.test uni0041
C Something
"""

f.lib["com.typesupply.ufo2fdk.glyphOrderAndAliasDB"] = goadb
f.save()
```

> To apply the GOADB on font generation it is necessary to turn *Release Mode* **ON**.
{: .warning }
