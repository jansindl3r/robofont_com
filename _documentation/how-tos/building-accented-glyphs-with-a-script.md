---
layout: page
title: Building accented glyphs with a script
tags:
  - accents
  - components
level: beginner
---

* Table of Contents
{:toc}

## Using GlyphConstruction

RoboFont 3 comes with the `glyphConstruction` module embedded, so you can import it into a script and build glyphs from glyph construction rules.

{% showcode building-tools/font/buildAccentedGlyphsRF3.py %}

## Using compileGlyph (RoboFont 1)

In RoboFont 1, you can build accented glyphs using RoboFab’s `RFont.compileGlyph`.

{% showcode building-tools/font/buildAccentedGlyphsRF1.py %}

## Upgrading from compileGlyph to glyphConstruction

If you are upgrading from RoboFont 1 to RoboFont 3, you can use the script below to convert your accents data into a string of glyph constructions.

{% showcode how-tos/accentsDict2GlyphConstruction.py %}
