---
layout: page
title: Printing proofs with DrawBot
tags:
  - proofing
  - drawBot
level: intermediate
draft: true
---

Have a look at these example scripts from the [Building Tools](../../building-tools) chapter:

- {% internallink 'building-tools/toolspace/drawbot/proof-character-set' %}
- {% internallink 'building-tools/toolspace/drawbot/proof-spacing' %}
- {% internallink 'building-tools/toolspace/drawbot/proof-glyphs' %}
- {% internallink 'building-tools/toolspace/drawbot/proof-features' %}
