---
layout: page
title: Drawing with quadratic curves
tags:
  - TrueType
  - contours
level: intermediate
draft: true
draft-hidden: true
---

* Table of Contents
{:toc}

## Implementation of quadratic beziers in RoboFont

RoboFont offers a simplified implementation of quadratic contours, with 2 off-curve points for all quadratic segments. This implementation is very similar to the one in {% glossary FogQ %} – not a coincidence, since both FontQ and the initial version of RoboFont were commissioned by David Berlow.

This approach to quadratic curves makes it possible to maintain some structural compatibility between the two curve types. When converting from cubic to quadratic, RoboFont adds an (implied) on-curve point and splits each curve in two parts.

{% image how-tos/drawing-with-quadratic-curves_cubic-quadratic.png %}

Other cubic-to-quadratic conversion algorithms are possible using code libraries or extensions.

> - {% internallink 'how-tos/converting-from-cubic-to-quadratic' %}
{: .seealso }

## Cubic or quadratic, not both

While it’s possible to have mixed cubic and quadratic contours in the same UFO, this is strongly discouraged in RoboFont.

If mixed contour types are found in a font, a dialog to *Resolve curve conflicts* will appear: you can use it to convert all glyphs to cubic or quadratic.

{% image how-tos/drawing-with-quadratic-curves_mixed-curve-types.png %}

## Default curve type

The default Bézier curve type can be set in the {% internallink 'workspace/preferences-window/glyph-view' %}.

{% image how-tos/drawing-with-quadratic-curves_preferences-bezier-type.png %}

Use the pop-up button at the top-right of the *Display* section to choose between:

- *Cubic (PostScript)*
- *Quadratic (TrueType)*

This setting applies to all new and open fonts.

## Display colors for quadratic points

The color of quadratic points in the Glyph Editor can also be configured in the {% internallink 'workspace/preferences-window/glyph-view' %}. Look for the following settings in the section *Appearance*:

- Off-curve Stroke Color (Quadratic Beziers, TrueType)
- Handle Stroke Color (Quadratic Beziers, TrueType)

The same two settings are also available in the {% internallink 'workspace/preferences-editor' %}:

- `glyphViewHandlesQuadStrokeColor`
- `glyphViewOffCurveQuadPointsStroke`

{% image how-tos/drawing-with-quadratic-curves_s-cubic.png caption='cubic curves (PostScript)' %}

{% image how-tos/drawing-with-quadratic-curves_s-quadratic.png caption='quadratic curves (TrueType)' %}

> Choose distinctive colors for PostScript and TrueType points, to avoid confusion between the two.
{: .tip }
