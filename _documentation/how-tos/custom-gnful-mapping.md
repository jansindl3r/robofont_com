---
layout: page
title: Generating and using custom GNFUL
tags:
  - unicode
  - character set
level: advanced
draft: true
draft-hidden: true
---

[GNUFL, Glyph Namer Formatted Unicode List](http://github.com/LettError/glyphNameFormatter) is glyph name - unicode generator.


```python
from glyphNameFormatter.exporters.exportFlatLists import generateFlat

gnfulPath = "path/to/your/gnful.txt"

generateFlat(gnfulPath, scriptSeparator=":")
```

use that file in the {% internallink "workspace/preferences-window/character-set" %}.