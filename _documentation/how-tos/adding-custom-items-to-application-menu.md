---
layout: page
title: Adding custom items to the application menu
tags:
  - scripting
  - UI
level: intermediate
draft: true
---

> - [Easy way to replicate Scripts menu bar? (RoboFont Forum )](https://forum.robofont.com/topic/612/easy-way-to-replicate-scripts-menu-bar)
{: .seealso }
