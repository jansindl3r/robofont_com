---
layout: page
title: Generating fonts
tags:
  - OpenType
  - scripting
level: beginner
draft: false
---

* Table of Contents
{:toc}

## Supported output formats

RoboFont can generate fonts in the following binary formats:

OpenType CFF
: ^
  - PostScript-flavored OpenType fonts
  - `.otf` file extension
  - cubic outlines

OpenType TTF
: ^
  - TrueType-flavored OpenType fonts
  - `.ttf` file extension
  - quadratic outlines

PostScript
: ^
  - `.pfa` file extension
  - cubic outlines
  - legacy format

Other font formats are supported using extensions or external libraries:

Variable fonts + webfonts
: Variable fonts and all webfont formats (WOFF, WOFF2, EOT, SVG) can be generated using the [Batch] extension.

Color fonts
: OpenType color fonts (SVG, COLR/CPAL and sbix formats) can be generated using the [RoboChrome] extension.

VFB fonts
: Source fonts in FontLab’s `.vfb` format can be generated directly from the *Generate Fonts* sheet if [vfb2ufo] is installed.

[RoboChrome]: http://github.com/jenskutilek/RoboChrome
[vfb2ufo]: http://blog.fontlab.com/font-utility/vfb2ufo/
[Batch]: http://github.com/typemytype/batchRoboFontExtension

## Generating fonts from the menu

To generate the current font, choose *File > Generate Font* from the {% internallink 'workspace/application-menu' %} (or use the shortcut keys ⌥ ⌘ G) to open the *Generate Font* sheet:

{% image how-tos/generating-fonts_sheet.png %}

### Options

<table>
  <thead>
    <tr>
        <th width='35%'>option</th>
        <th width='65%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Save As</td>
      <td>The name of the output font file.</td>
    </tr>
    <tr>
      <td>Tags</td>
      <td>Tags to be assigned to the generated file. (optional)</td>
    </tr>
    <tr>
      <td>Where</td>
      <td>Folder where the file should be generated.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>layer</td>
      <td>Choose a UFO layer as the source for the generated font.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>decompose</td>
      <td>Convert all components into contours.</td>
    </tr>
    <tr>
      <td>remove overlap</td>
      <td>Remove overlaps in the outlines of all glyphs.</td>
    </tr>
    <tr>
      <td>autohint</td>
      <td>Apply the AFDKO’s autohint program to the font.</td>
    </tr>
    <tr>
      <td>release mode</td>
      <td>Set release mode ON. This turns on subroutinization, applies the <code>GlyphOrderAndAliasDB</code> file, and removes “Development” from the version string.
</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>format</td>
      <td>Choose a format for the generated font.</td>
    </tr>
    <tr>
      <td>use MacRoman as start of the glyph order</td>
      <td>Use the MacRoman character set as the first glyphs in the font.</td>
    </tr>
  </tbody>
</table>

## Generating fonts with a script

Use the [`font.generate`] method to generate fonts with code:

[`font.generate`]: ../../building-tools/api/fontParts/rfont#RFont.generate

```python
f = CurrentFont()

formats = {
  'OpenType-CFF (otf)' : 'otfcff',
  'OpenType-TTF (ttf)' : 'otfttf',
  'PostScript (pfa)'   : 'pctype1ascii',
}

for format in formats.keys():
    print('generating %s font...' % format)
    errors = f.generate(formats[format])
    # print(errors)
```

> - [Batch generate fonts for all UFOs in folder](../../building-tools/toolspace/scripts/scripts-font#batch-generate-fonts-for-all-ufos-in-folder)
> - {% internallink "using-test-install" %}
{: .seealso }
