---
layout: page
title: Asking for help
tags:
  - getting help
level: beginner
draft: false
---

If you have any questions about RoboFont, you can ask for help on the [RoboFont Forum]. We‘ll do our best to point you in the right direction. Other RoboFont users may also be able to help.

[RoboFont Forum]: http://forum.robofont.com/

Before posting your question, please read the recommendations below:

Do your homework.
: Try looking for answers in the documentation and on the forum first. Both have *Search* and *Tags* functionality which make it easier to find content about a particular topic.

Be clear and concise.
: Make it as easy as possible for others to understand the issue. This will increase your chances to get a quick and useful answer.

Be specific.
: Try to isolate the actual issue from everything else that is not relevant to your question.

Provide test code and/or data.
: This will help us to reproduce and understand the issue.

Be patient…
: It might take a few days before we can reply to your question.

> - {% internallink "dealing-with-errors" %}
> - {% internallink "submitting-bug-reports" %}
{: .seealso }
