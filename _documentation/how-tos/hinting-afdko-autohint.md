---
layout: page
title: Autohinting OT-CFF fonts with AFDKO
tags:
  - hinting
  - PostScript
level: intermediate
draft: true
---

...

> Postscript Hints – presentation by Miguel Sousa at RoboThon 2012
> - [video](http://vimeo.com/38364880)
> - [pdf](http://typekit.files.wordpress.com/2012/04/postscript_hints_robothon_2012.pdf)
{: .seealso }
