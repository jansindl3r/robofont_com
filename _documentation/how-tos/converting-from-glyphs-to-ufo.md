---
layout: page
title: Converting from Glyphs to UFO
tags:
  - UFO
draft: true
draft-hidden: true
---

* Table of Contents
{:toc}

[Glyphs] uses its own `.glyphs` file format to store font sources, and supports UFO as a data interchange format: UFOs can be opened and saved as a native format, and there’s a built-in UFO export function.

The Glyphs documentation includes a tutorial on [Working with UFOs][Working with UFO]. This page provides additional information for RoboFont users.

## Notes about exporting UFOs from Glyphs

1. When exporting UFOs using the *Export to UFO* menu, only the current front font is exported – active instances in files with multiple masters are not included. Use the [script](#) below if you need to export UFOs from all instances.

2. Fonts are exported to [UFO2] format; [UFO3] is not supported yet.

3. All glyphs are exported, including those marked to not export. You‘ll have to take care of removing them later.

4. The following private keys are added to the UFO’s font lib:

   - `com.schriftgestaltung.font.license`
   - `com.schriftgestaltung.font.licenseURL`
   - `com.schriftgestaltung.font.panose`
   - `com.schriftgestaltung.disablesAutomaticAlignment`
   - `com.schriftgestaltung.glyphOrder`
   - `com.schriftgestaltung.useNiceNames`
   - `com.schriftgestaltung.fontMasterID`

5. Mark colors are lost when opening exported UFOs in RoboFont 1; they work fine in RoboFont 3.

## Exporting UFOs with code

Below are a couple of Python scripts to export UFOs from Glyphs.

### Export all open fonts to UFO

This script exports the front master of all open fonts to UFO.

{% showcode how-tos/exportFontsFromGlyphsToUFO.py %}

### Export all instances to UFO

This script exports all instances in the current font to UFO.

{% showcode how-tos/exportInstancesFromGlyphsToUFO.py %}

## Opening UFOs in Glyphs

> - Is any type of data lost when opening UFOs in Glyphs?
{: .todo }

---

Based on original contribution by [Joancarles Casasín].

[Glyphs]: http://glyphsapp.com
[Working with UFO]: http://glyphsapp.com/tutorials/working-with-ufo
[UFO2]: http://unifiedfontobject.org/versions/ufo2/
[UFO3]: http://unifiedfontobject.org/versions/ufo3/
[Joancarles Casasín]: http://casasin.com/
