---
layout: page
title: Installing extensions with Mechanic 2
treeTitle: Installing with Mechanic 2
tags:
  - extensions
  - Mechanic
level: beginner
---

* Table of Contents
{:toc}

**Mechanic is a RoboFont extension which makes it easier to find, download, install/uninstall, and update other RoboFont extensions.**

> - {% internallink 'extensions/introducing-mechanic-2' %}
{: .seealso }

## Installing Mechanic 2

To use Mechanic you will need to install it first: download the [Mechanic 2 extension] from GitHub, and [install it manually](../installing-extensions).

Once installed, Mechanic 2 will appear in RoboFont’s *Extensions* menu.

## Finding extensions

Go to the *Extensions* menu and open the Mechanic 2 window. Mechanic will pull information from the registered extension streams, and display a list of available extensions.

{% image extensions/installing-extensions-mechanic_extensions-list.png %}

Title, developer name and a short description are shown for each extension. If available, an icon is also displayed, as well as additional information (current installation status, price, upgrades available etc).

{% comment %}
paid extensions
: the price of extensions from the Extension Store is shown in green

installed extensions
: a gray circle before the extension name indicates that the extension is installed

upgrade available
: the version number of the new version appears after its description

unofficial version
: a red warning is displayed if your version of an extension is not the official one
{% endcomment %}

### Filtering the extensions list

The search bar can be used to search for extension names, authors and tags.

The following special search words are supported:

`?installed?`
: shows all installed extensions

`?not_installed?`
: shows all extensions which are *not* installed

`?update?`
: shows all extensions for which updates are available

## Installing extensions

Mechanic 2 comes with [Mechanic][Mechanic website] and [Extension Store] as default extension streams. Other extension streams and single extension items can be added using the *Settings* sheet.

> - {% internallink 'extensions/managing-extension-streams' %}
{: .seealso }

When installing an extension, Mechanic will fetch the extension’s source code, and copy it to the appropriate folder in your system:

```text
~/Library/Application Support/RoboFont/plugins/
```

Installing free & open-source extensions
: To install an open-source extension, simply select it from the list, and click on the *Install* button.

Installing commercial extensions
: To install a paid extension you’ll need to buy it first. Select the extension from the list, and click the *Purchase* button – you will be taken a payment page in the Extension Store. After the payment process is completed, the extension will be downloaded and installed.

## Uninstalling extensions

To uninstall an extension, select it from the list, and click on the *Uninstall* button.

## Updating extensions

Use the *Check for Updates* button to check if there are any updates available for the installed extensions.

> Hold `Alt` while clicking on the button to check for updates *only for selected extensions* – this will speed up things when the list of extensions is very long.
{: .tip }

If an update is available, the grey circle to the left of the extension item will turn orange, and the current/available version numbers will be shown:

{% image extensions/installing-extensions-mechanic_update-available.png %}

To update an extension, select it and click on the *Update* button:

{% image extensions/installing-extensions-mechanic_install-update.png %}

A confirmation dialog will appear – choose *Re-Install* to upgrade.

{% comment %}
{% image extensions/installing-extensions-mechanic_reinstall-dialog.png %}
{% endcomment %}

[Mechanic 2 extension]: http://github.com/robofont-mechanic/mechanic-2
[Mechanic website]: http://robofontmechanic.com/
[Extension Store]: http://extensionstore.robofont.com/
