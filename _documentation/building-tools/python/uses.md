---
layout: page
title: Uses of Python
level: beginner
---

Organizations
: Many large organizations use Python, including Wikipedia, Google, CERN, NASA, Facebook, Amazon, Instagram, Spotify, Reddit, DropBox.

  > - [10 Major Uses of Python](http://www.dummies.com/programming/python/10-major-uses-of-python/)
  > - [Python Success Stories](http://www.python.org/about/success/)
  {: .seealso }

Science
: Libraries such as [NumPy], [SciPy], [Matplotlib] and [IPython] allow the use of Python for scientific computing in many fields.

[NumPy]: http://numpy.org/
[SciPy]: http://scipy.org/
[Matplotlib]: http://matplotlib.org/
[IPython]: http://ipython.org/

Web
: Several kinds of Python tools are available for creating websites, from frameworks like [Django] or [Flask], to static site generators like [Pelican] or [Lektor] and documentation tools like [Sphinx].

[Django]: http://djangoproject.com/
[Flask]: http://flask.pocoo.org/
[Pelican]: http://getpelican.com/
[Lektor]: http://getlektor.com/
[Sphinx]: http://sphinx-doc.org/

3D modelling
: Python is used as scripting language in 3D modelling applications such as [Blender], [Cinema 4D], [Rhinoceros] and [FreeCad].

[Blender]: http://docs.blender.org/manual/en/latest/advanced/scripting/introduction.html
[Cinema 4D]: http://www.maxon.net/en-us/products/cinema-4d/overview/
[Rhinoceros]: http://www.rhino3d.com/
[FreeCad]: http://www.freecadweb.org/

Hardware
: Python can be used to control [circuit boards][CircuitPython]!

[CircuitPython]: http://circuitpython.readthedocs.io/

Graphics
: Python can be used to create 2D graphics with [DrawBot], [Processing.py], [Even].

[DrawBot]: http://drawbot.com/
[Even]: http://xxyxyz.org/even/
[Processing.py]: http://py.processing.org/

Typeface design
: Python scripting is supported in [RoboFont], [FontLab], [Glyphs] and [FontForge].

Font production
: There are {% internallink 'building-tools/toolkit/libraries' text='open-source Python libraries' %} for handling nearly every aspect of font production.

[RoboFont]: http://robofont.com/
[FontLab]: http://fontlab.com/python-scripting/
[Glyphs]: http://docu.glyphsapp.com/
[FontForge]: http://www.fontforge.org/python.html
