---
layout: page
title: External modules
level: beginner
draft: true
---

* Table of Contents
{:toc}

## Some useful modules

- Pillow
- grapefruit


## Installing modules

### Installing modules with pip

```python
pip install markdown
```

### Installing modules with setup scripts

...

### Installing modules manually using .pth files

Create a simple text file in a code editor, containing the path to the root folder where the module lives.

> The easiest way to get the correct path is by dragging the folder from Finder into a code editor or Terminal – so you’ll get the path without having to type it.
{: .tip }

Save this file with the name of the module and the extension `.pth` in the `site-packages` folder for the desired Python(s). For example:

``` console
/Library/Python/2.7/site-packages/myModule.pth
```

And that’s it.

This method creates a *reference* to the folder in which the module lives.

## Testing a module

To see if a module is installed, just try to import it in the environment you wish to work in:

```python
import myModule
```

If no error is raised, you’re good to go.

> - include other useful external packages
> - expand installation instructions
> - inlude markdown, grapefruit, etc. examples?
{: .todo }

{% comment %}

[PyPI]: http://pypi.org/
[pip]: http://en.wikipedia.org/wiki/Pip_(package_manager)
[GitHub]: https://github.com/search?q=language:Python
[Python Standard Library]: http://docs.python.org/3.6/library/index.html

[grapefruit](http://github.com/xav/Grapefruit>)
: working with colors in different color modes, creating gradients etc.

[markdown](http://pythonhosted.org/Markdown/)
: generating html from Markdown sources

*some other module*
: *what the module can be used for*

{% endcomment %}
