---
layout: page
title: Python in DrawBot
level: beginner
---

* Table of Contents
{:toc}

DrawBot is an enviroment for creating graphics programatically in macOS using Python. It is open-source and can be downloaded for free from [drawbot.com](http://drawbot.com).

{% image python/drawbot-icon.png %}

Developed originally as an educational tool for teaching programming to designers, DrawBot has grown into a full-fledged platform for automated creation of all kinds of graphic material.

{% image python/drawbot.png %}

multiple output formats
: DrawBot can save vector graphics (SVG, PDF), raster images (PNG, JPEG, TIFF, GIF, BMP) and animations (GIF, mov, mp4).

advanced typography
: DrawBot offers a full range of typographic controls, including support for OpenType features and variable fonts, hyphenation, tabulation, access to font metrics etc.

interaction with code
: DrawBot includes a small set of UI controls – sliders, buttons, input fields, etc. – which can be used to add interaction to your scripts. The code editor has special [code interaction] features for some types of values, and a live coding mode which executes the code as you type.

[code interaction]: ../../../workspace/scripting-window#code-interaction

> - [DrawBot documentation](http://drawbot.com/)
> - [DrawBot source](http://github.com/typemytype/drawbot)
{: .seealso }

## DrawBot as RoboFont extension

DrawBot is also available as a [RoboFont extension][DrawBot extension]. Because it runs inside RoboFont, this version of DrawBot has access to the entire RoboFont API, including `fontParts.world` objects such as `CurrentFont`, `CurrentGlyph`, `AllFonts` etc. It has also a handy `drawGlyph(glyph)` function which draws an [RGlyph] object into the DrawBot canvas.

{% image python/drawbot-extension.png %}

[DrawBot extension]: http://github.com/typemytype/drawBotRoboFontExtension
[RGlyph]: ../../api/fontParts/rglyph/

> - [Installing extensions manually](../../../extensions/installing-extensions)
> - [Installing extensions with Mechanic 2](../../../extensions/installing-extensions-mechanic)
{: .seealso }

## DrawBot as a module

DrawBot can also be used as a Python module, without the UI. After you install the module, you can `import drawBot` into your scripts and create images with code.

> - [Using DrawBot as a Python module](http://github.com/typemytype/drawbot#using-drawbot-as-a-python-module)
{: .seealso }

## DrawBot package format

DrawBot scripts often require additional files such as image resources, Python modules, data files, etc. The `.drawbot` package format makes it easier to share and distribute such multi-file projects.

{% image python/drawbot-package-icon.png %}

DrawBot packages can be built using the Package Builder window included in DrawBot. Open a `.drawbot` file by double-clicking or dropping it on top of DrawBot.

> - [DrawBot Package](http://www.drawbot.com/content/drawBotApp/drawBotPackage.html)
{: .seealso }

## More DrawBot links

> - [DrawBot forum](http://forum.drawbot.com/)
>
> [Daily DrawBot](http://dailydrawbot.tumblr.com/)
> : Animated gifs made with Python in DrawBot, mostly by Just van Rossum
>
> [Rob Stenson — How to Say “Crunch” in Portuguese](http://ohnotype.co/blog/rob-stenson-interview)
> : Using DrawBot and variable fonts to design dynamic interfaces for multiple languages.
{: .seealso }
