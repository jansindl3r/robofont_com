---
layout: page
title: Language basics
level: beginner
---

* Table of Contents
{:toc}

Now that we know where to write Python code, let's have a look at the Python language.

## The look’n’feel of Python

Python is meant to be an easily readable language. Its formatting is visually uncluttered, and it often uses English keywords where other languages use punctuation.

Unlike many other languages, Python uses indentation instead of using braces for grouping statements. *In Python, whitespace is not optional.* This gives Python a characteristic clean, organized structure.

Instead of writing statements in C like this:

```c
if (a < b) {
    mmax = b;
} else {
    mmax = a;
}
```

Python dispenses braces altogether, along with the trailing semicolons:

```python
if a < b:
    mmax = b
else:
    mmax = a
```

> - [Interpreted Languages: A side-by-side reference sheet](http://hyperpolyglot.org/scripting)
{: .seealso }

## The print statement

The command `print` simply ‘prints’ the given argument in the console or output window.

This is all you need to write you first Python program, the famous ‘hello world’:

```python
print('hello world')
```

> The `print` statement is not needed when working in interactive mode – every expression is executed and returns its result.
{: .note }

## Comments

Comments are parts of your code that are not executed. They usually contain notes for your future self or for any other programmers reading the code. Comments can also be used to turn certain parts of code on/off during development.

Comments are marked by a `#` sign. Everything after the `#` is a comment and is not executed:

```python
# my first program!
print('hello world')
```

A comment can also appear in the middle of a line:

```python
print('hello world') # hi there, I'm a comment
```

## The None object

Most programming languages have a special object type to represent nothing, emptyness. In Python, this is the `None` object:

```python
a = None
```

Note that this is different from `0`, or an empty string `''`, or an empty list `[]`. `None` is the absence of a value of any type.

## The boolean object

The `bool` object is a special object which can have only two values, `True` or `False`. Booleans represent a binary state:

```python
>>> a = True
>>> type(a)
```

```console
<type 'bool'>
```

```python
>>> b = False
>>> type(b)
```

```console
<type 'bool'>
```

In Python, boolean values are always written with a capital first letter, so `true` or `false` will throw an error:

```python
>>> a = false
```

```console
Traceback (most recent call last):
  File "<untitled>", line 1, in <module>
NameError: name 'false' is not defined
```

> - {% internallink 'building-tools/python/boolean-logic' %}
{: .seealso }

## Object types

There are several different object types in Python. Here are the main ones:

- `str` *strings*
- `int` *integers*
- `float` *floating point numbers*
- `list` *lists*
- `tuple` *tuples*
- `dict` *dictionaries*
- `bool` *booleans*
- etc.

Each type of object has its own properties and methods. Strings are used to represent sequences of characters, integers represent real numbers, lists represent ordered collections of items etc. We’ll be taking a closer look at the main object types in Python in the next sections.

The built-in function `type()` returns the type of a given object:

```python
>>> type(None)
```

```console
<type 'NoneType'>
```

```python
>>> type('hello')
```

```console
<type 'str'>
```

```python
>>> type(123)
```

```console
<type 'int'>
```

```python
>>> type(10.5)
```
```console
<type 'float'>
```

```python
>>> type({'key':'value'})
```

```console
<type 'dict'>
```

```python
>>> type(True)
```

```console
<type 'bool'>
```

## Error messages

In Python, error messages are objects too – [Exception objects]. There are various kinds of exceptions, each one for a different situation. When an exception message appears, read it carefully: it usually contains information that will help you to understand and solve the problem.

[Exception objects]: http://docs.python.org/3.6/library/exceptions

Here are a few examples of exceptions. Don’t worry if you don’t understand them yet, just try to familiarize yourself with the different kinds of errors:

`NameError`
: ^
  Happens if we try to use a variable which has not been defined yet:

  ```python
  >>> b
  ```

  ```console
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  NameError: name 'b' is not defined
  ```

`ZeroDivisionError`
: ^
  Happens if we try to divide any number by zero:

  ```python
  >>> 1 / 0
  ```

  ```console
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  ZeroDivisionError: division by zero
  ```

`IndexError`
: ^
  Happens when we try to access an item by an index bigger than the length of the collection:

  ```python
  >>> L = ''
  >>> L[1]
  ```

  ```console
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  IndexError: string index out of range
  ```

`KeyError`
: ^
  Happens if we try to access a non-existing key from a dictionary:

  ```python
  >>> D = {}
  >>> D['key']
  ```

  ```console
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  KeyError: 'key'
  ```

`IndentationError`
: ^
  Happens when Python is expecting an indented block and doesn’t find one:

  ```python
  >>> for i in range(7):
  >>> i
  ```

  ```console
  Traceback (most recent call last):
    File "<stdin>", line 2
      i
      ^
  IndentationError: expected an indented block
  ```

And a few other ones:

- `TypeError`
- `AssertionError`
- `ImportError`
- `SyntaxError`
