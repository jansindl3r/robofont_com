---
layout: page
title: Boolean logic
level: beginner
---

* Table of Contents
{:toc}

Logical operators are typically used to evaluate whether two or more expressions are `True` or `False`.

## The 'and' operator

The `and` operator evaluates if two or more statements are `True`. It returns `True` only if all expressions are `True`; in all other cases it returns `False`.

<table>
  <tr>
    <td width='34%'><code>and</code></td>
    <th width='33%'><code>True</code></th>
    <th width='33%'><code>False</code></th>
  </tr>
  <tr>
    <th><code>True</code></th>
    <td><code class='green'>True</code></td>
    <td><code class='red'>False</code></td>
  </tr>
  <tr>
    <th><code>False</code></th>
    <td><code class='red'>False</code></td>
    <td><code class='red'>False</code></td>
  </tr>
</table>

In the example below, we want to do something only if *both values* are bigger than `15`, otherwise we'll do something else:

```python
a = 17 # try different values here
b = 17
if a > 15 and b > 15:
    print("do something")
else:
    print("do something else")
```

## The 'or' operator

The `or` operator evaluates if at least one among several statements is `True`. It returns `False` only if all expressions are `False`; in all other cases it returns `True`.

<table>
    <tr>
        <td width='34%'><code>or</code></td>
        <th width='33%'><code>True</code></th>
        <th width='33%'><code>False</code></th>
    </tr>
    <tr>
        <th><code>True</code></th>
        <td><code class='green'>True</code></td>
        <td><code class='green'>True</code></td>
    </tr>
    <tr>
        <th><code>False</code></th>
        <td><code class='green'>True</code></td>
        <td><code class='red'>False</code></td>
    </tr>
</table>

In the example below, we’ll do something if *at least one value* is bigger than `15`, otherwise we’ll do something else:

```python
a = 17 # try different values here
b = 17
if a > 15 or b > 15:
    print("do something")
else:
    print("do something else")
```

## The 'not' operator

The `not` operator simply inverts the value of an expression: if the expression is `True`, it returns `False`; if the expression is `False`, it returns `True`.

<table>
  <tr>
    <th width='50%'><code>True</code></th>
    <th width='50%'><code>False</code></th>
  </tr>
  <tr>
    <td><code class='red'>False</code></td>
    <td><code class='green'>True</code></td>
  </tr>
</table>

A simple example:

```python
>>> a = 300 > 12
>>> a
```

```console
True
```

```python
>>> not a
```

```console
False
```

The `not` operator is often used in combination with the keyword `in` for testing item membership.

In this next example we print out the strings which are in `L1` but not in `L2`:

```python
>>> L1 = ['a', 'b', 'c', 'd', 'e', 'f']
>>> L2 = ['a', 'e', 'i', 'o', 'u']
>>> for char in L1:
...     if char not in L2:
...         char
```

```console
b
c
d
f
```

<!--

## Grouping expressions

Like aritmetic expressions, boolean expressions can be also grouped using parentheses to indicate the order of the operations.

```python
if (a > b and b == 13) or b == 25:
    # do something
```

```python
if a > b and (b == 13 or b == 25):
    # do something
```

-->
