---
layout: page
title: Python in Terminal
level: beginner
---

* Table of Contents
{:toc}

Python can be used directly in the console as a command-line interface. On macOS, this is done using the Terminal – you can find it in your *Applications / Utilities* folder.

{% image python/terminal-icon.png %}

After launching Terminal, you will see something like this:

```text
Last login: Wed Jul 23 05:01:17 on ttys000
username:~ username$
```

This is your default command-line prompt, with your own username.

There are two ways to use Python in the console:

1. interactive mode
2. running existing scripts

## Interactive mode

To enter interactive mode in Terminal, simply type `python` in the command-line:

```text
username:~ username$ python
```

You will probably see something like this:

```console
>>> Python 2.7.5 (default, Aug 25 2013, 00:04:04)
>>> [GCC 4.2.1 Compatible Apple LLVM 5.0 (clang-500.0.68)] on darwin
>>> Type "help", "copyright", "credits" or "license" for more information.
>>>
```

This command gives us some information about the current Python installation, and returns an interactive Python prompt.

> The default Python in macOS is still Python 2.7.X. See [Installing Python 3 on your system](../../how-tos/upgrading-from-py2-to-py3/#installing-python3-on-your-system) if you wish to use Python 3 in Terminal.
{: .note }

Once in the Python prompt, we can start typing code, and it will get executed.

Let's start with a simple *hello world*:

```python
>>> "hello world"
```

```console
'hello world'
```

> In interactive mode, it is not necessary to use the `print()` function – every expression is executed and returns its result.
{: .note }

> Throughout this documentation, code examples with lines starting in `>>>` are intended as interactive sessions in Terminal. All other examples are scripts.
{: .tip}

The same is valid to mathematical expressions (or for any Python expression):

```python
>>> 1 + 1
```

```console
2
```

```python
>>> 'x' in 'abcdef'
```

```console
False
```

It’s also possible to write multi-line code in interactive mode. Notice how the prompt changes to indicate that the interpreter is waiting for input:

```python
>>> for i in range(4):
...    i
...
```

```console
0
1
2
3
```

### User input

Interactive mode allows us to prompt the user for input while we are running a programming session. This can be done using the built-in function `input()`, which returns the string that was entered by the user.

```python
>>> answer = input('What… is your quest?' )
```

<!--
```console
To seek the Holy Grail.
```
-->

## Running existing scripts

The interactive mode is useful for writing quick tests and short scripts, but it is not really suitable for working with larger pieces of code. In that case, it makes more sense to write scripts as separate `.py` files, and use Terminal only to run execute them.

As an example, let’s suppose we have a Python script which prints `hello world` as output:

```python
print('hello world')
```

This script is saved in the desktop as `hello.py`.

To execute this file in Terminal we use the command `python` followed by the path to the script file:

```text
username:~ username$ python /Users/username/Desktop/hello.py
```

> Instead of typing out the full path, you can also drag the file from Finder to the Terminal prompt to get its path.
{: .tip }

The output will be, as expected:

```console
hello world
```

- - -

> - [Command line and environment](http://docs.python.org/3.6/using/cmdline.html)
> - [Using the Python Interpreter](http://docs.python.org/3.6/tutorial/interpreter.html)
> - [Interactive Input Editing and History Substitution](http://docs.python.org/3.6/tutorial/interactive.html#)
{: .seealso }
