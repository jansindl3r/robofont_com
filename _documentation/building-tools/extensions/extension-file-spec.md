---
layout: page
title: Extension File Specification
treeTitle: Extension File Spec
tags:
  - extensions
level: intermediate
---

* Table of Contents
{:toc}

RoboFont extensions are [macOS packages] – folders which act like single files. They have a [standard folder structure](#extension-folder-structure) and the file extension `.roboFontExt`.

[macOS packages]: http://en.wikipedia.org/wiki/Package_(macOS)

> You can view the contents of a package by right-clicking it in Finder, and selecting *Show Package Contents*.
{: .tip }


Extension Folder Structure
--------------------------

*Version: 2.0*

Extension packages must have the following internal structure:

> myExtension.robofontExtension
> ├── [info.plist](#infoplist)
> ├── [lib](#lib)
> │   └── \*.py
> ├── ([html](#html))
> │   ├── index.html
> │   └── (\*.html)
> ├── ([resources](#resources))
> │   └── (any type of file)
> ├── ([license](#license))
> └── ([requirements.txt](#requirements))
{: .asCode }

Files and folders in parentheses are not required.


info.plist
----------

The `info.plist` is a [XML property list] containing extension metadata.

[XML property list]: http://en.wikipedia.org/wiki/Property_list

> - [Technical specification of the Property List format as DTD](http://www.apple.com/DTDs/PropertyList-1.0.dtd)
{: .seealso }

<table>
<tr>
  <th width='45%'>key</th>
  <th>value</th>
</tr>
{% for data in site.data.extensionsSpec.extensionBundle %}
<tr>
  <td rowspan='3'>
    <code class='highlighter-rouge'>{{ data.key }}</code>
  </td>
  <td>{{ data.description | markdownify }}</td>
</tr>
<tr>
  <td><code>{{ data.value }}</code></td>
</tr>
<tr>
  <td class="{{ data.required | slugify }}">{{ data.required | markdownify }}</td>
</tr>
{% endfor %}
</table>

The `info.plist` can also include custom keys if necessary.

Custom entries should use **reverse domain names** as keys to avoid namespace conflicts with other extensions.

Example: `com.myDomain.myExtension.myCustomKey`


> In RoboFont 1, extension metadata for Mechanic was stored in the `info.plist` of each extension under the custom key `com.robofontmechanic.mechanic`.
>
> **This custom key is no longer used in RoboFont 3 / Mechanic 2.**
>
> Mechanic 2 introduces a different system for publishing extensions, with extension metadata stored centrally in the [Mechanic 2 Server].
>
> See {% internallink 'building-tools/extensions/publishing-extensions' %} for more information.
{: .note }

[Mechanic 2 Server]: http://github.com/robofont-mechanic/mechanic-2-server


lib
---

A folder containing all `.py` files needed by the extension.

When an extension is installed, its `lib` folder is added to the `sys.path` – so all the other files and folders in the extension can be imported as modules with Python. To avoid namespace conflicts with other extensions, it is recommended to use **reverse domain names without dots** to prefix extension modules.

Example: `comMyDomainMyExtensionMyModule`

> If a developer does not want to ship readable source code, it is possible to include only compiled `.pyc` files in the extension.
>
> **These `.pyc` files must be made with Python {{site.data.versions.python}} (built-in).**
>
> See the option *Collect only .pyc files* in {% internallink 'building-tools/extensions/building-extensions-with-extension-builder' %} (if you are building the extension manually), or the keyword `pycOnly` in {% internallink 'building-tools/extensions/building-extensions-with-script' %}.
{: .note }


html
----

not required
{: .not-required}

If an html folder is declared in the `info.plist`, then it must contain a file named `index.html`. This file must be a plain html file.

> [RoboFont 3.3b]({{site.baseurl}}/downloads/)
{: .version-badge }

The html folder may contain [Markdown] files (`.md`) instead of html. When building the extension, all markdown files are converted to html using a standard html/css template.

> Use the *Help* link in the extension’s submenu to open the html help in RoboFont’s {% internallink "building-tools/toolspace/mojo/help-window" %} (a simple [WebKit] browser).
{: .tip }

[WebKit]: http://www.webkit.org/
[Markdown]: http://en.wikipedia.org/wiki/Markdown

resources
---------

not required
{: .not-required}

The `resources` folder is a place to store any additional files needed for your extension. It is commonly used for assets (such as images for toolbar icons and cursors), or for additional compiled command-line tools.


license
-------

not required
{: .not-required}

The license file contains the full license text for the extension.

This file must be either in `.txt` or in `.html` format.


requirements
------------

not required
{: .not-required}

The `requirements.txt` file contains a list of other extensions which also need to be installed for the current extension to work.

For example, an extension which requires [DrawBot] and [Batch] should include a `requirements.txt` file with the names of both extensions, one per line:

```text
Batch
DrawBot
```

[Batch]: http://github.com/typemytype/batchRoboFontExtension
[DrawBot]: http://github.com/typemytype/drawBotRoboFontExtension


Menu Item Description
---------------------

When installed, an extension can add entries to the Extensions menu. These entries are defined as a list of *menu item descriptions*.

Each menu item description is a dictionary with the following keys:

<table>
<tr>
  <th width='25%'>key</th>
  <th>value</th>
</tr>
{% for data in site.data.extensionsSpec.menuItemDescription %}
<tr>
  <td rowspan='3'>
    <code class='highlighter-rouge'>{{ data.key }}</code>
  </td>
  <td>{{ data.description | markdownify }}</td>
</tr>
<tr>
  <td>{{ data.value }}</td>
</tr>
<tr>
  <td class="{{ data.required | slugify }}">{{ data.required | markdownify }}</td>
</tr>
{% endfor %}
</table>
