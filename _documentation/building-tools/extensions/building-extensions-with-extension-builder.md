---
layout: page
title: Building extensions with the Extension Builder
treeTitle: Building with the Extension Builder
tags:
  - extensions
level: beginner
---

The {% internallink "workspace/extension-builder" %} is a tool for creating and editing extension packages.

To build an extension, you’ll first need to fill in the required information in the dialog. You can also use the *Import* button to import data from an existing extension. See the {% internallink "building-tools/extensions/extension-file-spec" %} for details about each field.

After all the data is set, use the *Build* button to build the extension package.

The image below shows the settings used to build the {% internallink "boilerplate-extension" text='Boilerplate Extension' %}:

{% image building-tools/extensions/extensionBuilder.png %}
