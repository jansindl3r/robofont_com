---
layout: page
title: Scripts to do things to fonts
tags:
  - scripting
  - FontParts
level: intermediate
---

* Table of Contents
{:toc}

A collection of simple scripts to do things to fonts.

## Set fixed width in all glyphs

{% showcode building-tools/font/setFixedWidth.py %}

{% showcode building-tools/font/setFixedWidthUndo.py %}

## Set infos in all open fonts

{% showcode building-tools/font/setInfosDict.py %}

## Copy font infos from one font to another

{% showcode building-tools/font/copyFontInfo.py %}

## Batch generate fonts for all UFOs in folder

{% showcode building-tools/font/batchGenerateFonts.py %}

## Import a font into layer of the current font

{% showcode building-tools/font/importFontIntoLayer.py %}

## Building accented glyphs

{% showcode building-tools/font/buildAccentedGlyphsRF3.py %}

{% showcode building-tools/font/buildAccentedGlyphsRF1.py %}

{% comment %}
- subset OTF/TTF fonts using fontTools
- convert from cubic to quadratic: http://forum.robofont.com/topic/291/converting-ttf-splines-to-otf-splines/2
{% endcomment %}
