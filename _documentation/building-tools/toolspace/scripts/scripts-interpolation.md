---
layout: page
title: Interpolation scripts
tags:
  - interpolation
  - FontParts
level: intermediate
---

* Table of Contents
{:toc}

> - {% internallink "how-tos/interpolation-introduction" %}
{: .seealso }

## Interpolating colors

{% image building-tools/interpolation/interpolate-colors.png %}

{% showcode building-tools/interpolation/interpolateColors.py %}

## Interpolating position and size

{% image building-tools/interpolation/interpolate-pos-size.png %}

{% showcode building-tools/interpolation/interpolatePosSize.py %}

## Checking interpolation compatibility

Before interpolating two glyphs, we need to make sure that they are compatible. We can do that with code using a glyph’s `isCompatible` method. This function returns two values:

- The first is a boolean indicating if the two glyphs are compatible.
- If the first value is `False`, the second will contain a report of the problems.

```python
f = CurrentFont()
g = f['O']
print(g.isCompatible(f['o']))
```

```console
(True, '')
```

```python
print(g.isCompatible(f['n']))
```

```console
(False, '[Fatal] Contour 0 contains a different number of segments.\n[Fatal] Contour 1 contains a different number of segments.\n[Warning] The glyphs do not contain components with exactly the same base glyphs.')
```

> - {% internallink "how-tos/preparing-for-interpolation" %}
{: .seealso }

## Interpolating glyphs in the same font

{% image building-tools/interpolation/interpolate-in-font.png %}

{% showcode building-tools/interpolation/interpolateGlyphs.py %}

## Interpolating between two masters

{% showcode building-tools/interpolation/interpolateGlyphsIntoCurrentFont.py %}

## Interpolating fonts

{% showcode building-tools/interpolation/interpolateFonts.py %}

## Batch interpolating fonts

{% showcode building-tools/interpolation/batchInterpolateFonts.py %}

```console
generating instances...

   generating Light (0.25)...
   saving instances at /myFolder/MyFamily_Light.ufo...

   generating Regular (0.5)...
   saving instances at /myFolder/MyFamily_Regular.ufo...

   generating Bold (0.75)...
   saving instances at /myFolder/MyFamily_Bold.ufo...

...done.
```

## Condensomatic

{% image building-tools/interpolation/condensomatic.png %}

{% showcode building-tools/interpolation/condensomatic.py %}
