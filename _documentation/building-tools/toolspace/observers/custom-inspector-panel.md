---
layout: page
title: Custom Inspector section
tags:
  - observers
level: advanced
---

This example shows how to add a custom section to the {% internallink 'workspace/inspector' %} panel.

{% image building-tools/observers/custom-inspector-panel.png %}

{% showcode building-tools/observers/customInspectorPanel.py %}

> - {% internallink 'toolspace/mojo/accordion-window' %}
{: .seealso }