---
layout: page
title: Draw info text in the Glyph View
tags:
  - observers
level: advanced
---

This example shows how to draw informative text into the {% internallink 'workspace/glyph-editor' %} canvas using `glyphView.drawTextAtPoint`, so it is displayed in the same style as other ‘native’ text items such as point/segment/contour indexes, component info, etc.

{% image building-tools/observers/drawTextAtPointExample.png %}

{% showcode building-tools/observers/drawTextAtPointExample.py %}
