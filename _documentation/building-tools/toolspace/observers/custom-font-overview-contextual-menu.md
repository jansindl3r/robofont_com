---
layout: page
title: Custom contextual menu in the Font Overview
tags:
  - mojo
level: intermediate
---

This example shows how to create a custom contextual menu in the {% internallink 'workspace/font-overview' %}.

{% image building-tools/observers/customFontOverviewContextualMenu.png %}

> To activate the contextual menu, right-click anywhere in the font window.
{: .note }

{% showcode building-tools/observers/customFontOverviewContextualMenu.py %}

> The same approach can be used to add custom items to other contextual menus in RoboFont. Use the following keys to access the menus for glyphs, guides and images:
>
> - `glyphAdditionContextualMenuItems`
> - `guideAdditionContextualMenuItems`
> - `imageAdditionContextualMenuItems`
{: .tip }
