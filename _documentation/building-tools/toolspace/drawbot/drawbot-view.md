---
layout: page
title: Using DrawBot with vanilla
tags:
  - scripting
  - drawBot
level: intermediate
---

DrawBot can be used together with {% internallink 'toolspace/vanilla/vanilla-intro' text='vanilla' %} too!

Use the `drawView` to integrate a DrawBot document into your dialogs and tools, or to add an interface around your DrawBot scripts.

{% image building-tools/drawbot/drawbot-vanilla.png %}

{% showcode building-tools/drawbot/drawBotVanillaExample.py %}

> - {% internallink 'building-tools/toolspace/mojo/canvas' %}
{: .seealso }