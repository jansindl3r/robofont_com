---
layout: page
title: Proof spacing
tags:
  - scripting
  - drawBot
level: intermediate
---

This example shows how to create a simple proof using an installed test font and a generated list of test strings.

{% image building-tools/drawbot/proofSpacing.png %}

The script works by calling `font.testInstall()` to install a test version of the current font locally, so it can be used by the operating system.

> This line is commented out because you don’t need to generate and install a font every time you run the script; only when you wish to update to font.
{: .note }

The script then generates test strings for the main groups of characters in a font: lowercase, uppercase, digits, punctuation. These character groups are imported from the `string` module, and combined using *list comprehensions* to produce basic test strings.

{% showcode building-tools/drawbot/proofSpacing.py %}

> This proof can be extended by adding other test strings to list.
{: .tip }
