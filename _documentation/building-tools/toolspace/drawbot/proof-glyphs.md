---
layout: page
title: Proof single glyphs
tags:
  - scripting
  - drawBot
level: intermediate
---

This example generates a proof of all selected glyphs in the current font (or all glyphs, if no glyph is selected), one glyph per page.

{% image building-tools/drawbot/proofGlyphs.png %}

Each page shows a single glyph in the center of the page, and additional data such as font metrics, glyph box, anchors, glyph name, unicode, width, and left/right margins.

Page size, glyph scale, caption color and caption size can be adjusted in the variables at the top of the script.

{% showcode building-tools/drawbot/proofGlyphs.py %}

> This example could be extended to display other types of data, such as on-curve and off-curve points, blue zones, etc.
{: .tip }
