---
layout: page
title: Interactive tools
level: advanced
tree:
  - simple-tool-example
  - polygonal-selection-tool
---

RoboFont’s {% internallink 'workspace/glyph-editor' %} comes with a few core tools for manipulating glyphs: the [Editing], [Bezier], [Slice] and [Measurement] tools. These interactive tools are built around macOS [NSEvent] objects, which represent input actions such as mouse clicks and position, pressed keys, etc.

Custom interactive tools can be created by subclassing the `BaseEventTool` or one of the other core tools which are available from the {% internallink 'building-tools/api/mojo/mojo-events' text='`mojo.events`' %} module:

```python
# subclass base tool
from mojo.events import BaseEventTool
# or one of the core tools
from mojo.events import EditingTool, BezierDrawingTool, SliceTool, MeasurementTool
```

## Examples

{% tree page.url levels=1 %}

[Editing]: ../../../workspace/glyph-editor/tools/editing-tool
[Bezier]: ../../../workspace/glyph-editor/tools/bezier-tool
[Slice]: ../../../workspace/glyph-editor/tools/slice-tool
[Measurement]: ../../../workspace/glyph-editor/tools/measurement-tool
[NSEvent]: http://developer.apple.com/documentation/appkit/nsevent

> - [PixelTool](http://github.com/typemytype/pixelToolRoboFontExtension)
> - [ShapeTool](http://github.com/typemytype/shapeToolRoboFontExtension)
> - [RoboLasso](http://github.com/productiontype/RoboLasso)
> - [ScaleEditTool](http://github.com/klaavo/scalingEditTool)
> - [BlueZoneEditor](http://github.com/andyclymer/BlueZoneEditor-roboFontExt)
> - [BoundingTool](http://github.com/FontBureau/fbOpenTools/tree/master/BoundingTool)
> - [CheckParallelTool](http://github.com/jtanadi/CheckParallelTool)
> - [CornerTool](https://github.com/roboDocs/CornerTools)
{: .seealso }
