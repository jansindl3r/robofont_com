---
layout: page
title: Canvas and CanvasGroup
tags:
  - mojo
level: advanced
---

* Table of Contents
{:toc}

A canvas view is useful for creating dynamic previews and interactive interfaces. Imagine [DrawBot] with a live-updating draw loop, and with the ability to react to events such as mouse position and clicks, keyboard input, etc.

This model may sound familiar to users of interactive graphic environments like [Processing]; it’s also how the native macOS UI layer [Cocoa] works. Canvas is just a thin pythonic wrapper around Cocoa’s [NSView] class.

## How canvas works

The `Canvas` and `CanvasGroup` objects inherit behavior from the `NSView` class, which controls the presentation and interaction of visible content in a macOS application.

Both canvas objects have a `delegate` method which can receive notifications of user actions *(events)*. When an action is sent to the delegate *(notification)*, it triggers a delegate method associated with it *(callback)*. This system makes it possible to update the canvas view in real-time based on user input.

A view is refreshed when an update is requested. An update request can be triggered by a user action (such as a window resize) or programmatically by calling `Canvas.update()`.

An update request doesn’t immediately refresh the view; it turns on a [flag][needsDisplay] which tells the view that it needs to be redrawn before being displayed. The actual redraw is handled by the operating system in an efficient way – for example, it does not happen when the view is not visible on screen.

## Events

An *event* is an object which contains information about an input action such as a mouse click or a key press. Depending on the type of event, we can ask where the mouse was located, which character was typed, etc. Some attributes are common to all events, while others are specific to certain types of events. The [Canvas] and [CanvasGroup] documentations include a list of all delegate events which can be used by the canvas objects.

> - [NSEvent]
> - [Event Objects and Types]
{: .seealso }

## Types of canvas

There are two slightly different types of canvas views in `mojo.canvas`:

[Canvas]
: ^
  A [vanillaScrollView] with a canvas view inside.

  The edges of the drawing have scrollbars. The canvas size is limited.

[CanvasGroup]
: ^
  A [vanillaGroup] with a canvas view inside.

  The edges of the drawing are bound to the edges of the window.

## Canvas vs. DrawView

With `Canvas` and `CanvasGroup` objects, we are drawing directly to the screen while the view is being refreshed. This is very fast, but also fragile: an error in the drawing can crash the whole application.

DrawBot’s `DrawView` is an alternative for cases in which speed and interactivity play a lesser role. Instead of drawing in the main program loop like `Canvas`, it uses a two-step process: first the PDF data is generated, and then it is set in the view. This process is not so fast, but a bit more robust.

> - {% internallink 'toolspace/drawbot/drawbot-view' %}
> - [mojo.canvas crashing?](http://forum.robofont.com/topic/444/mojo-canvas-crashing)
{: .seealso }

[Canvas]: ../../../api/mojo/mojo-canvas/#mojo.canvas.Canvas
[CanvasGroup]: ../../../api/mojo/mojo-canvas/#mojo.canvas.CanvasGroup
[DrawBot]: http://drawbot.com/
[Processing]: http://processing.org/
[Cocoa]: http://en.wikipedia.org/wiki/Cocoa_(API)
[Event Objects and Types]: http://developer.apple.com/library/content/documentation/Cocoa/Conceptual/EventOverview/EventObjectsTypes/EventObjectsTypes.html
[NSEvent]: http://developer.apple.com/documentation/appkit/nsevent
[NSView]: http://developer.apple.com/documentation/appkit/nsview?language=objc
[NSScrollView]: http://developer.apple.com/documentation/appkit/nsscrollview
[vanillaScrollView]: http://ts-vanilla.readthedocs.io/en/latest/objects/ScrollView.html
[vanillaGroup]: http://ts-vanilla.readthedocs.io/en/latest/objects/Group.html
[needsDisplay]: http://developer.apple.com/documentation/appkit/nsview/1483360-needsdisplay?language=objc

## Examples

### Canvas vs. CanvasGroup

This example helps to compare `Canvas` and `CanvasGroup`. Resize the windows to see the difference.

{% showcode building-tools/mojo/canvasExample.py %}

### Canvas events

This example shows a simple canvas-based ‘brush’ tool using mouse and keyboard events.

{% image building-tools/mojo/CanvasBrush.png %}

{% showcode building-tools/mojo/canvasBrushExample.py %}

### Using canvas with vanilla

In this example, the canvas reacts to an event triggered by another UI component (slider) in the same window. The slider callback calls the `Canvas.update()` method, which tells the canvas view to refresh itself.

{% showcode building-tools/mojo/canvasUIExample.py %}

### Canvas animation

This example shows an animation using `Canvas`.

The canvas view is continuously updated using a timer (`NSTimer`). The timer triggers a custom `redraw` callback, which updates the canvas view and schedules the next timer event to happen after a given time interval.

{% showcode building-tools/mojo/canvasAnimationExample.py %}

> - [Animation in mojo.Canvas (RoboFont Forum)](http://forum.robofont.com/topic/396/animation-in-mojo-canvas)
{: .seealso }
