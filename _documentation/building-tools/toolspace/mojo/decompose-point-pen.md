---
layout: page
title: Using the DecomposePointPen
tags:
  - mojo
  - pens
level: intermediate
---

The script below shows how to decompose a glyph using the `DecomposePointPen`.

This approach to decomposing glyphs can be useful when the target glyph doesn’t have access to the parent font.

{% showcode building-tools/mojo/decomposePointPenExample.py %}
