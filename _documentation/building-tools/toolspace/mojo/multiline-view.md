---
layout: page
title: Multi-Line View
tags:
  - mojo
level: intermediate
---

The Multi-Line view is a reflowable text preview area for use with vanilla, with support for zoom and glyph selection. This is the same component used to display glyphs in the {% internallink 'workspace/space-center' %}.

{% image building-tools/mojo/MultiLineView.png %}

{% showcode building-tools/mojo/multiLineViewExample.py %}
