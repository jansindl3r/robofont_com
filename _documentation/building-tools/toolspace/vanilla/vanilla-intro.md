---
layout: page
title: Introduction to vanilla
tags:
  - vanilla
level: beginner
---

* Table of Contents
{:toc}

## What is vanilla?

[vanilla] is Python wrapper around Apple’s native [Cocoa] UI layer. It offers a {% glossary pythonic %} API for creating interfaces with code, rather than using visual tools like Apple’s [Interface Builder]. vanilla was developed by Tal Leming, and was inspired by the classic {% glossary W %} library used by {% glossary RoboFog %}.

## How to use vanilla

vanilla is embedded in RoboFont, so you can start using it in your scripts right away:

```python
from vanilla import *
```

vanilla has a very complete [documentation][vanilla documentation] which is available directly from your scripting environment:

```python
from vanilla import Window
help(Window)
```

The above snippet will return the provided documentation for the `Window` object, including some sample code:

```python
from vanilla import *

class WindowDemo(object):

    def __init__(self):
        self.w = Window((200, 70), "Window Demo")
        self.w.myButton = Button((10, 10, -10, 20), "My Button")
        self.w.myTextBox = TextBox((10, 40, -10, 17), "My Text Box")
        self.w.open()

WindowDemo()
```

Run the sample code to open a demo window:

{% image building-tools/vanilla/WindowDemo.png %}

## Widgets overview

Below is an overview of the UI elements included in vanilla. These images are screenshots from vanilla’s own test files, which are included in the library.

```python
from vanilla.test.testAll import Test
Test()
```

{% image building-tools/vanilla/vanilla-tests_1.png %}

{% image building-tools/vanilla/vanilla-tests_2.png %}

{% image building-tools/vanilla/vanilla-tests_3.png %}

[Cocoa]: http://en.wikipedia.org/wiki/Cocoa_(API)
[Interface Builder]: http://developer.apple.com/xcode/interface-builder/
[vanilla]: http://github.com/robotools/vanilla
[vanilla documentation]: http://ts-vanilla.readthedocs.io/

> - [Tal Leming introducing vanilla (RoboThon 2012)](http://vimeo.com/116064787#t=17m00s)
{: .seealso}
