---
layout: page
title: Moving a glyph using sliders
tags:
  - scripting
  - vanilla
level: intermediate
---

This example shows a simple tool with sliders to move the current glyph.

The trick here is to store the slider movements in a variable, so we can compare the desired total distance with the current one.

{% image building-tools/vanilla/windowWithMoveSliders.png %}

{% showcode /building-tools/vanilla/simpleMoveExample.py %}
