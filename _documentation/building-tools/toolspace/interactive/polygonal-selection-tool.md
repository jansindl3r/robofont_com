---
layout: page
title: Polygonal selection tool
---

This example shows a polygonal selection tool based on the [EditingTool](../../../api/mojo/mojo-events/#mojo.events.EditingTool).

{% showcode building-tools/interactive/polygonSelection.py %}

