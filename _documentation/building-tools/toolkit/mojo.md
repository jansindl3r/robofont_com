---
layout: page
title: mojo
tags:
  - scripting
  - mojo
tree:
  - ../../api/mojo/*
treeCanHide: true
level: intermediate
---

The `mojo` module gives access to RoboFont-specific objects, events and tools.

{% tree "/documentation/building-tools/api/mojo/*" levels=1 "%}

> - {% internallink "/building-tools/toolspace/mojo" %}
{: .seealso }
