---
layout: page
title: Building extensions
tree:
  - extension-file-spec
  - from-tool-to-extension
  - boilerplate-extension
  - building-extensions-with-extension-builder
  - building-extensions-with-script
  - packaging-patterns
  - publishing-extensions
treeCanHide: true
tags:
  - extensions
level: intermediate
---

In addition to its {% internallink 'documentation/building-tools/toolkit' text='open and documented APIs' %}, RoboFont also offers an Extensions infrastructure which makes it easy for developers to build and distribute extensions, and for users to install and update them.

{% comment %}
{% image extensions/extension-icon.png %}
{% endcomment %}

In this section you’ll find everything you need to know about building and distributing RoboFont extensions.

- {% internallink "extension-file-spec" %}
- {% internallink "from-tool-to-extension" %}
- {% internallink "boilerplate-extension" %}
- {% internallink "building-extensions-with-extension-builder" %}
- {% internallink "building-extensions-with-script" %}
- {% internallink "publishing-extensions" %}
- {% internallink "packaging-patterns" %}

{% comment %}
- {% internallink "recommendations-publishing" %}
- {% internallink "publishing-commercial-extensions" %}
{% endcomment %}
