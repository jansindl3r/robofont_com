---
layout: page
title: RoboFont features overview
treeTitle: Features overview
level: beginner
---

* Table of Contents
{:toc}

## Editing

{% internallink "workspace/font-overview" %}
: Visualizing a font’s glyph set as a grid of cells or as a table.

{% internallink "workspace/glyph-editor" %}
: Creating and editing glyphs. Can be highly customized and extended.

{% internallink "workspace/groups-editor" %}
: Creating and editing font-specific groups of glyphs.

{% internallink "workspace/smart-sets" text="Smart Sets" %}
: User defined groups of glyphs, saved together with your user preferences.

## Spacing & Kerning

{% internallink "workspace/space-center" %}
: Spacing and proofing glyphs, creating text samples.

{% internallink "workspace/kern-center" %}
: Creating and editing font kerning data.

## Customizing

{% internallink "workspace/preferences-window" %}
: An interface to configure several types of user preferences.

{% internallink "workspace/preferences-editor" %}
: Editing several other configuration values directly.

[Extensions](#)
: Using, installing, developing and publishing open-source and commercial extensions.

## Mastering

{% internallink "workspace/features-editor" %}
: Writing and editing OpenType features code.

[Feature Preview](http://github.com/typemytype/featurePreviewRoboFontExtension)
: Extension to preview features in a UFO font (generate a test otf font).

{% internallink "workspace/font-info-sheet" text='Font Info' %}
: Editing several kinds of font metadata.

{% internallink "how-tos/generating-fonts" %}
: Generating OpenType CFF/TTF fonts and additional formats.

## Proofing

{% internallink "how-tos/using-test-install" text="Test Install" %}
: Install the current font into the system for testing.

{% internallink "how-tos/printing-proofs-with-drawbot" text="DrawBot extension" %}
: Creating all kinds of visual font samples in various formats.

## Scripting

{% internallink "workspace/scripting-window" %}
: Writing Python code to do something to fonts.

{% internallink "workspace/output-window" %}
: Viewing text output from scripts, debugging.

## Licensing

{% internallink "licensing" text="Licenses" %}
: Several licensing options for students, teachers, professionals and companies.

{% internallink "workspace/license-window" %}
: Easy drag-and-drop license activation with the License Window.
